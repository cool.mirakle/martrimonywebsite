<?php namespace App\Models;

use CodeIgniter\Model;

class UserSubscription extends Model
{
    protected $table      = 'user_subscription';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    //protected $useSoftDeletes = true;

    protected $allowedFields = ['user_id', 'sub_start_date','sub_end_date','status','is_trial','inserted_date_time','updated_date_time','paidAmount','refImage','reference_number','plan_name','plan_amount','plan_id'];

    protected $useTimestamps = true;
    //protected $createdField  = '';
    //protected $updatedField  = '';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    
}