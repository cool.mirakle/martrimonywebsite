<?php namespace App\Models;

use CodeIgniter\Model;

class ProfileDetails extends Model
{
    protected $table      = 'user_profile_details';
    protected $primaryKey = 'id';
   protected $foreignKey = 'user_id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    //protected $useSoftDeletes = true;

    protected $allowedFields = ['first_name', 'last_name','middle_name','age','email_id','phone_no','dob','gender','profile_pic','religion','star','created_for','approved_by','property_details','address','caste','education','occupation','salary','own_house','subcaste','language','state','nationality','height','weight','drinker','drinking_level','smoker','smoking_level','jadhakam','rasi','profile_description','hobbies','foodies','blood_group'];

    protected $useTimestamps = true;
    //protected $createdField  = '';
    //protected $updatedField  = 'updated_date_time';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}  