<?php namespace App\Models;

use CodeIgniter\Model;

class PlanDetails extends Model
{
    protected $table      = 'plans';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    //protected $useSoftDeletes = true;

    protected $allowedFields = ['plan_name', 'price','inserted_date','updated_date','isActive','description'];

    protected $useTimestamps = true;
    //protected $createdField  = '';
    //protected $updatedField  = '';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    
}