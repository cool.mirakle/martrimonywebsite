<?php namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;

use CodeIgniter\Model;
use App\Models\CommonModel;

class FileHandlingModel extends Model
{
    
    public function __construct()
    {
        helper(['form', 'url']);
    }

    public function paymentUpload($file ,$reference ,$plan_id,
    $plan_amount,$plan_name,
    $paidAmount){

        $sessionModel = new SessionModel();

        $userId = $sessionModel->getUserIdFromSession();

        if($userId) {
            $newName = $userId.$reference.$file->getRandomName();

            $file->move(WRITEPATH.'uploads', $newName);

            //return $planDetails;
            $toInsert = [
                'user_id' => $userId,
                'plan_id'=>$plan_id,
                'plan_name'=>$plan_name,
                'plan_amount'=>$plan_amount,
                'reference_number'=>$reference,
                'refImage'=>$newName,
                'paidAmount'=>$paidAmount,
                'status'=>1//sent for verification
            ];

            //return $toInsert;

            // insert into subscription table 
            // plan Id , plan Amount , Paid Amount , Plan Name , Reference Number , RefImagePath , userId , status = 0

            $userSubscription = new UserSubscription();        

            $id = $userSubscription->where('user_id', $userId)->findColumn('id');
            
            if($id)
            {
                $result = db_connect()->table('user_subscription')->where('user_id',$userId)->update($toInsert) ?["status"=>true,"message"=>"Payment Details Sent For Verification","data"=>[]]:["status"=>false,"message"=>"Payment Details Verification Failed. Try Again","data"=>[]];

            } else {
                                
                $result = db_connect()->table('user_subscription')->insert($toInsert) > 0 ?["status"=>true,"message"=>"Payment Details Sent For Verification","data"=>[]]:["status"=>false,"message"=>"Payment Details Verification Failed. Try Again","data"=>[]];
                
            }

            return $result;

        } else {
            return ["status"=>false,"message"=>"Authentication Failed","data"=>[] ];
        }
        
    }

    public function uploadFile($file , $docTypeId , $documentName , $userName){
       
        $newName = $documentName.$file->getRandomName();

        $file->move(WRITEPATH.'uploads', $newName);

        $commonModel = new CommonModel();

        $data = $commonModel->getUserLoginDetails($userName);

        if($docTypeId == 1){

            $toInsert = [
                'profile_pic' => $newName
            ];

            $isSuccess = db_connect()->table('user_profile_details')->where('user_id', $data[0]->{'id'})->update($toInsert);

            if($isSuccess){
                return ['message'=>"Successfully updated profile","status"=>true,"data"=>[]];
            } else {
                return ['message'=>"Profile Update Failed","status"=>false,"data"=>[]];
            }
            
        } else {

            $docData = [
                "doc_type"=>$docTypeId,
                "doc_name"=>$newName,
                "type"=>$documentName,
                "user_id"=>$data[0]->{'id'}
            ];

            $isSuccess = db_connect()->table('user_documents')->insert($docData);

            if($isSuccess){
                return ['message'=>"Document Updated successfully","status"=>true,"data"=>[$data[0]->{'id'}]];
            } else {
                return ['message'=>"Document Update Failed","status"=>false,"data"=>[]];
            }

        }

    }

}