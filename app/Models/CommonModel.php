<?php namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;

use CodeIgniter\Model;

use CodeIgniter\I18n\Time;

use \Firebase\JWT\JWT;

class CommonModel extends Model
{
    
    public function __construct()
    {
        helper(['form', 'url']);
    }

    public function calculateAge($from){

        $bday = Time::parse($from);
        
        $datetime = Time::createFromDate();

        $diff = $datetime->diff($bday);
        
        return $diff->y; 
    }

    public function checkAge($gender,$dob){
        if($gender == 'Male' ) {

            return $this->calculateAge($dob)>=21;

        } else if($gender == 'Female' ) {
            
            return $this->calculateAge($dob)>=18;

        } else {
            return false;
        }
    }

    public function getUserLoginDetails($username){
        
        $query =  db_connect()->table('user_dtls')->getWhere(['user_name' => $username])->getResult();//.getWhere(['user_name' => $username], 1, 1)->get();
                
        return $query;
    }

    public function getUserLoginDetailsById($userId){
        
        $query =  db_connect()->table('user_dtls')->getWhere(['id' => $userId])->getResult();//.getWhere(['user_name' => $username], 1, 1)->get();
                
        return $query;
    }

    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); 
        $alphaLength = strlen($alphabet) - 1; 
        for ($i = 0; $i < 12; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

    public function encodeToken($payload)
    {
        $jwtToken = $this->objOfJwt->GenerateToken($payload);
        echo json_encode(array('Token'=>$jwtToken));
        
    }
            
    public function decodeToken()
    {
        $received_Token = $this->input->request_headers('Authorization');
        try
        {
            $jwtData = $this->objOfJwt->DecodeToken($received_Token['Token']);
            echo json_encode($jwtData);
        }
        catch (Exception $e)
        {
            http_response_code('401');
            echo json_encode(array( "status" => false, "message" => $e->getMessage()));exit;
        }
    }

    public function sendEmail($to,$subject,$message){

        $email = \Config\Services::email();

        $email->clear();
        $email->setFrom('crmmiraklesa1@gmail.com', 'Mirakle Yabaze');
        $email->setTo($to);
        
        $email->setSubject($subject);
        $email->setMessage($message);

        return $email->send();
    }

    function arrayToObject(array $array, string $class_name){

        $r = new ReflectionClass($class_name);
        $object = $r->newInstanceWithoutConstructor();
        $list = $r->getProperties();
        foreach($list as $prop){
          $prop->setAccessible(true);
          if(isset($array[$prop->name]))
            $prop->setValue($object, $array[$prop->name]);
        } 
    
        return $object;
    
    }

}