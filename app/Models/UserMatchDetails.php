<?php namespace App\Models;

use CodeIgniter\Model;

class UserMatchDetails extends Model
{
    protected $table      = 'user_match_details';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    //protected $useSoftDeletes = true;

    protected $allowedFields = ['from_user_id', 'to_user_id','inserted_date_time','updated_date_time','status'];

    protected $useTimestamps = true;
    //protected $createdField  = '';
    //protected $updatedField  = '';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    
}