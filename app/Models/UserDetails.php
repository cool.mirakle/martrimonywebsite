<?php namespace App\Models;

use CodeIgniter\Model;

class UserDetails extends Model
{
    protected $table      = 'user_dtls';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    //protected $useSoftDeletes = true;

    protected $allowedFields = ['password', 'is_active'];

    protected $useTimestamps = true;
    protected $createdField  = 'inserted_date_time';
    protected $updatedField  = 'updated_date_time';

    //protected $validationRules    = [];
    //protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $validationRules    = [
        'username'        => 'required|valid_email|is_unique[user_dtls.username]'
    ];

    protected $validationMessages = [
        'username'        => [
            'is_unique' => 'Sorry. That email has already been taken. Please choose another.'
        ]
    ];
}