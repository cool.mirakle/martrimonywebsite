<?php namespace App\Models;

use CodeIgniter\Model;

class PatnerExpectation extends Model
{
    protected $table      = 'patner_expectation';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    //protected $useSoftDeletes = true;

    protected $allowedFields = ['user_id', 'age_from','age_to','height_from','height_to','religion','cast','education','employment_status','income_status','income_from','income_to','marital_status','description'];

    protected $useTimestamps = true;
    //protected $createdField  = '';
    //protected $updatedField  = '';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    
}