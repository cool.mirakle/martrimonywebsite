<?php namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;

use CodeIgniter\Model;

use \Firebase\JWT\JWT;

$key = base64_decode("mirakleyabaze");

class AuthModel extends Model
{
    
    public function __construct()
    {
        helper(['form', 'url']);
    }

    public function authenticate($username, $password){

        if(empty($username) || empty($password )){   
            $jwtToken = JWT::encode(["username"=>$username, "password"=>$password], $key);

            return ["message"=>"Provide email and password.","status"=>FALSE,"token"=>$jwtToken];

        }  else {

            $loginDetails['username'] = $username;
            $loginDetails['password'] = $password;

            $result = $this->checkCredentials($loginDetails);

            $jwtToken = JWT::encode($result['data'], $key,'HS256');
            $user_id = $result['data']['user_id'];
            $profile_id = $result['data']['profile_id'];
            $user_role = $result['data']['user_role'];
            unset($result['data']);

            $result['token'] = $jwtToken;

            if($result['status']){

                $this->session = \Config\Services::session();
                $this->session->set('token',$jwtToken);
                $this->session->set('user_id',$user_id);  
                $this->session->set('profile_id',$profile_id);  
                $this->session->set('user_role',$user_role);  
                
                return $result;


            } else {
                return $result;
            }
        }
        
     }

     public function checkCredentials($data){

        $commonModel = new CommonModel();
        $result = $commonModel->getUserLoginDetails($data['username']);
        
        $response = [];

        if(sizeOf($result)<=0){
            $response['message']='user Details Not Found';
            $response['status'] = FALSE;
            $response['data']=[];

        } else {

            if($result[0]->{'is_active'} != 5 && $result[0]->{'is_active'} != 6) {
                
                $value = password_verify($data['password'],$result[0]->{'password'});

                if($value){
                    
                    $response['data'] = [
                        "user_id"=>$result[0]->{'id'},
                        "user_role"=>db_connect()->table('user_roles')->getWhere(['user_id' => $result[0]->{'id'}])->getResult()[0]->{'user_role'},
                        "profile_id"=>$result[0]->{'profile_id'}
                    ];

                    $response['message']='Login Successfully';
                    
                    $response['status'] = TRUE;

                } else {
                    $response['message']='Check Your Credentials';
                    $response['status'] = FALSE;
                    $response['data']=[];
                }

            }  else {
                $response['message']='Your Profile is Deactivated.';
                $response['status'] = FALSE;
                $response['data']=[];
            }

        }

        return $response;

    }

    public function logout(){

        session_destroy();

        $result['status'] = true;
        $result['message'] = 'Logout Successfully';
        $result['data'] = [];

        return $result;
    }

    public function registerUser($data){

        $commonModel = new CommonModel();

        $userDetails['user_name'] = $data->{'user_name'};
        $hashed_password = password_hash($data->{'password'},PASSWORD_DEFAULT);

        $userDetails['password'] = $hashed_password;    

        if($commonModel->checkAge($data->{'gender'},$data->{'dob'})){

            $result = $this->inserUserDetails($userDetails,$data);

        } else {
            $result['status'] = False;
            $result['message'] = 'Please Wait until you reach your Age. Anyway Hearty Congratulations. :)';
            $result['data'] = [];
        }
    
        return $result;

    }

    public function inserUserDetails($data,$profileDetails){

        $commonModel = new CommonModel();
        $result =   $commonModel->getUserLoginDetails($data['user_name']);
        
        $response = [];

         if(sizeOf($result)>0){
            $response['message']='Usernam Already Taken. Try With Some Other Username';
            $response['status'] = FALSE;
            $response['data'] = [];

            return $response;

        } else {
            
            $query = db_connect()->table('user_dtls')->insert($data);

            $result = $commonModel->getUserLoginDetails($data['user_name']);
            
            if(sizeOf($result) >0){    

                $profileData['user_id'] = $result[0]->{'id'};
                $profileData['email_id'] = $result[0]->{'user_name'};
                $profileData['first_name'] = $profileDetails->{'firstName'};
                $profileData['middle_name'] = $profileDetails->{'middleName'};
                $profileData['last_name'] = $profileDetails->{'lastName'};

                $profileData['age'] = $commonModel->calculateAge($profileDetails->{'dob'});
                $profileData['phone_no'] = $profileDetails->{'phone_number'};
                $profileData['dob'] = $profileDetails->{'dob'};

                $profileData['gender'] = $profileDetails->{'gender'};
                $profileData['created_for'] = $profileDetails->{'register_for'};
                $profileData['religion'] = $profileDetails->{'religion'};
                
                $query = db_connect()->table('user_profile_details')->insert($profileData);

                $role['user_id'] = $result[0]->{'id'};

                $role = db_connect()->table('user_roles')->insert($role);

                if($query>0){
                    $response['message'] = "Thank You For Registration. We are processing your registration";
                    $response['status'] = TRUE;
                    $response['data'] = [];
                } else {
                    $response['message'] = "Thank You For Registration. We Will Contact You soon";
                    $response['status'] = TRUE;
                    $response['data'] = [];
                }

            } else {
                $response['message'] = 'Registration Failed';
                $response['status'] = FALSE;
                $response['data'] = [];
            }
        }
                
        return $response;
    }

    public function forgotPassword($data){
        $commonModel = new CommonModel();
        $result =   $commonModel->getUserLoginDetails($data->{'username'});
        
        if(sizeOf($result) > 0){
            
            $randomPassword =  $commonModel->randomPassword();

            $hashed_password = password_hash($randomPassword,PASSWORD_DEFAULT);
            
            $userDetails = new UserDetails();

            $data = [
                'id'       => $result[0]-> {'id'},
                'password' => $hashed_password
            ];

            $result = $userDetails->save($data);

            if($result){

                $updatePasswordStatus = $commonModel->sendEmail($result[0]-> {'user_name'},"Password Reset","Password Rest Successfully. Your Password is <strong>".$randomPassword.'</strong>');
               
                if($updatePasswordStatus){

                    return ["message"=>"Password Sent To your Registered Email Address.","status"=>true,"response"=>$emailResponse];
                } else {
                    return ["message"=>"Password Updated Successfully. ","status"=>TRUE];
                }

            } else {
                return ["message"=>"Password Update Failed","status"=>FALSE];
            }

            return $result;

        } else {
            return ["message"=>"Provide Login Information.","status"=>FALSE];
        }
    }

    public function changePasswordByUserId($userId,$oldPassword,$newPassword){

        $sessionModel = new SessionModel();

        if($sessionModel->checkUserIdWithSession($userId)){
            return ["message"=>"Password Update Failed","status"=>FALSE];
        } 
        // if(!$sessionModel->validateToken()){
        //     return ["status"=>false,"message"=>"Authentication Failed","data"=>[] ];
        // }

        $commonModel = new CommonModel();
        $result = $commonModel->getUserLoginDetailsById($userId);
        if(sizeOf($result) > 0){

            $value = password_verify($oldPassword,$result[0]->{'password'});

            if($value){
                
                $hashed_password = password_hash($newPassword,PASSWORD_DEFAULT);
            
                $userDetails = new UserDetails();

                $data = [
                    'id'       => $result[0]-> {'id'},
                    'password' => $hashed_password
                ];

                $result = $userDetails->save($data);

                if($result){

                    $updatePasswordStatus = $commonModel->sendEmail($result[0]-> {'user_name'},"Password Changed Alert","Password Cahnged Successfully. Your Password is <strong>".$newPassword.'</strong>');
                
                    if($updatePasswordStatus){

                        return ["message"=>"Password Cahnge Status Sent To your Registered Email Address.","status"=>true,"response"=>$emailResponse];
                    } else {
                        return ["message"=>"Password Changed Successfully. ","status"=>TRUE];
                    }

                } else {
                    return ["message"=>"Password Update Failed","status"=>FALSE];
                }

            } else {
                $response['message']='Check Your Old Password';
                $response['status'] = FALSE;
                $response['data']=[];
                return $response;
            }

        }  else {
            return ["message"=>"User Details Not Found","status"=>FALSE];
        }
    
    }

    public function resetProfilePasswordById($userId,$newPassword){

        $commonModel = new CommonModel();
        $sessionModel = new SessionModel();

        $result = $commonModel->getUserLoginDetailsById($userId);

        // if(!$sessionModel->validateToken()){
        //     return ["status"=>false,"message"=>"Authentication Failed. Please Re-Login.","data"=>[] ];
        // }

        if(sizeOf($result) > 0){
                
                $hashed_password = password_hash($newPassword,PASSWORD_DEFAULT);
            
                $userDetails = new UserDetails();

                $data = [
                    'id'       => $result[0]-> {'id'},
                    'password' => $hashed_password
                ];

                $result = $userDetails->save($data);

                if($result){

                    $updatePasswordStatus = $commonModel->sendEmail($result[0]-> {'user_name'},"Password Changed Alert","Password Cahnged Successfully. Your Password is <strong>".$newPassword.'</strong>');
                
                    if($updatePasswordStatus){
                        return ["message"=>"Password Reset Successfully. Details Sent To the Registered Email Address.","status"=>true,"response"=>$emailResponse];
                    } else {
                        return ["message"=>"Password Reset Successfully. ","status"=>TRUE];
                    }

                } else {
                    return ["message"=>"Password Update Failed","status"=>FALSE];
                }


        }  else {
            return ["message"=>"User Details Not Found","status"=>FALSE];
        }
    }

    public function updatePaymentStatus($userId,$status){

        $sessionModel = new SessionModel();
        if($sessionModel->checkAdmin()){
            
            $toInsert = [
                'user_id' => $userId,
                'status'=>$status
            ];

            $userSubscription = new UserSubscription();        

            $id = $userSubscription->where('user_id', $userId)->findColumn('id');
            
            if($id)
            {
                $result = db_connect()->table('user_subscription')->where('user_id',$userId)->update($toInsert) ?["status"=>true,"message"=>"Payment Status Updated","data"=>[]]:["status"=>false,"message"=>"Payment Status Update failed","data"=>[]];

            } else {
                                
                $result = db_connect()->table('user_subscription')->insert($toInsert) > 0 ?["status"=>true,"message"=>"Payment Status Updated Successfully","data"=>[]]:["status"=>false,"message"=>"Payment Status Update failed. Try Again","data"=>[]];
                
            }

            return $result;


        } else {
            return ["message"=>"You cannot update the payment status of a user","status"=>FALSE];
        }
    }

    public function resetProfileStatus($userId,$status){

        $commonModel = new CommonModel();
        $sessionModel = new SessionModel();

        // if(!$sessionModel->validateToken()){
        //     return ["status"=>false,"message"=>"Authentication Failed","data"=>[] ];
        // }

        $result = $commonModel->getUserLoginDetailsById($userId);

        if(sizeOf($result) > 0){
            $userDetails = new UserDetails();

                $data = [
                    'id'       => $result[0]-> {'id'},
                    'is_active' => $status
                ];

                $result = $userDetails->save($data);

                if($result){

                    $updatePasswordStatus = $commonModel->sendEmail($result[0]-> {'user_name'},"Profile Update","Your Profile Status Changed Successfully.");
                
                    if($updatePasswordStatus){
                        return ["message"=>"Profile Status Updated Successfully. Details Sent To the Registered Email Address.","status"=>true,"response"=>$emailResponse];
                    } else {
                        return ["message"=>"Profile Status Updated Successfully. ","status"=>TRUE];
                    }

                } else {
                    return ["message"=>"Profile Status Update Failed","status"=>FALSE];
                }
        }  else {
            return ["message"=>"User Details Not Found","status"=>FALSE];
        }

    }

}