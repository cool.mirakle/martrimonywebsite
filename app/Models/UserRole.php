<?php namespace App\Models;

use CodeIgniter\Model;

class UserRole extends Model
{
    protected $table      = 'user_roles';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    //protected $useSoftDeletes = true;

    protected $allowedFields = ['user_id', 'user_role'];

    protected $useTimestamps = true;
    //protected $createdField  = '';
    //protected $updatedField  = '';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    
}