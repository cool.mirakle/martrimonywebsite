<?php namespace App\Models;

use CodeIgniter\Model;

class FamilyDetails extends Model
{
    protected $table      = 'family_details';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'App\Entities\FamilyEntity';
    //protected $useSoftDeletes = true;

    protected $allowedFields = ['user_id', 'father_name','mother_name','family_income','father_occupation','mother_occupation','family_description'];

    protected $useTimestamps = true;
    //protected $createdField  = '';
    //protected $updatedField  = '';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    
}