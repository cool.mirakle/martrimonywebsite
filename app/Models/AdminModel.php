<?php namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;

use CodeIgniter\Model;
use App\Models\FamilyDetails;

class AdminModel extends Model
{
    
    public function __construct()
    {
        helper(['form', 'url']);

    }

    public function getAllUserDetails(){

        $sessionModel = new SessionModel();

        // if(!$sessionModel->validateToken()){
        //     return ["status"=>false,"message"=>"Authentication Failed","data"=>[] ];
        // }

        if($sessionModel->checkAdmin()){
            //admin process;
            $sql = "select * from user_profile_details as a,user_roles as b,user_dtls as c where a.user_id=b.user_id and a.user_id in (SELECT id FROM matrimony.user_dtls where is_active in(0,1,2,3,4)) and b.user_role='ROLE_USER' and c.id = a.user_id;";   

            $query = db_connect()->query($sql);
            $row   = $query->getResult();
            foreach($row as $r){
                unset($r->password);
            }
            return ["message"=>"Success","status"=>TRUE,"data"=>$row];

        } else {
            //not a admin;
            $userId = $sessionModel->getUserIdFromSession();

            if($userId) {
                $userSubscription = new UserSubscription();

                $userSubscriptionData = $userSubscription->where(['status'=>2,'user_id'=>$userId])->first();
                $verificationPending = $userSubscription->where(['status'=>1,'user_id'=>$userId])->first();
                $paymentRejected = $userSubscription->where(['status'=>3,'user_id'=>$userId])->first();
                
                if($paymentRejected){
                    return ["status"=>false,"message"=>"Payment Rejected. Contact Reginal Office","data"=>[]];
                }

                if($verificationPending){
                    return ["status"=>false,"message"=>"Payment Verification Pending","data"=>[]];
                }

                if($userSubscriptionData){

                    //plan Id and based on plan id 
                    // return user Details based on plan
                    $sql="select * from user_profile_details as a,user_roles as b,user_dtls as c where a.user_id=b.user_id and a.user_id in (SELECT id FROM matrimony.user_dtls where is_active in(0,1,2,3,4)) and b.user_role='ROLE_USER' and c.id = a.user_id;";   

                    $query = db_connect()->query($sql);
                    $row   = $query->getResult();
                    foreach($row as $r){
                        unset($r->password);
                    }
                    return ["message"=>"Success","status"=>TRUE,"data"=>$row];
                    //needs to add one more condition here
                    
                } else {
                    return ["status"=>false,"message"=>"Please Subscribe Plan","data"=>[]];
                }

            } else {
                return ["status"=>false,"message"=>"Authentication Failed","data"=>[] ];
            }
            // check plan status
            // if plan is active then return user Details
            // else return plan details

        }

        
    }

    public function updateSubscriptionDetails($planDetails){

        //check userId is Available or not
        $userSubscription = new UserSubscription();
        $sessionModel = new SessionModel();

        $userId = $sessionModel->getUserIdFromSession();

        $userSubscriptionData = $userSubscription->where('user_id', $userId)->find();

        $subScriptionData = [
            "user_id"=>$userId,
            "status"=>1,
            "plan_id"=>$planDetails->{'id'}
        ];

        if(empty($userSubscriptionData)){

            $result = db_connect()->table('user_subscription')->insert($subScriptionData) > 0 ?["status"=>true,"message"=>"Plan Subscription Completed Successfully","data"=>[]]:["status"=>false,"message"=>"Plan Subscription Failed","data"=>[]];
                    
            return $result;

        } else {

            $id = $userSubscription->where('user_id', $userId)->findColumn('id');

            $deactivate = [
                "user_id"=>$userId,
                "status"=>0
            ];

            $result = db_connect()->table('user_subscription')->where('user_id',$userId)->update($deactivate) ?["status"=>true,"message"=>"Plan Subscription Updated Successfully","data"=>[]]:["status"=>false,"message"=>"Plan Subscription Update Failed","data"=>[]];

            $result = db_connect()->table('user_subscription')->insert($subScriptionData) > 0 ?["status"=>true,"message"=>"Plan Subscription Completed Successfully","data"=>[]]:["status"=>false,"message"=>"Plan Subscription Failed","data"=>[]];

            return $result;
        }

    }

    public function getActivePlanDetails(){
        
        $planDetails = new PlanDetails();
        $response['data']['planDetails'] = $planDetails->where('isActive',1)->find();

        $response['status'] = true;
        $response['message'] = "Success";

        return $response;
    }

    public function getUserFullProfile($profileId){

        $userDetails = new UserDetails();
        $profileDetails = new ProfileDetails();
        $familyDetails = new FamilyDetails();
        $userRole = new UserRole();
        $patnerExpectation = new PatnerExpectation();
        $userSubscription = new UserSubscription();
        $planDetails = new PlanDetails();
        $userDocument = new UserDocument();
        $userMatchDetails = new UserMatchDetails();
        $transactionDetails = new TransactionDetails();
        $userOrders = new UserOrders();

        $id = $userDetails->where('profile_id', $profileId)->findColumn('id');

        if($id){

            $userDetail = $userDetails->where('profile_id', $profileId)->first();

            unset($userDetail->{'password'});

            $response['data']['userDetails'] = $userDetail;
            $response['data']['profileDetails'] =  $profileDetails->where('user_id',  $id)->first();
            $response['data']['familyDetails'] = $familyDetails->where('user_id', $id)->first();
            $response['data']['userRole'] = $userRole->where('user_id', $id)->first();
            $response['data']['patnerExpectation'] = $patnerExpectation->where('user_id', $id)->first();
            $response['data']['userSubscription'] = $userSubscription->where(['user_id'=>$id])->first();

            $planId = $userSubscription->where(['user_id'=>$id])->findColumn('plan_id');
            
            if($planId){
                
                $response['data']['planDetails'] = $planDetails->where('id',$planId)->first();
                
            }

            $response['data']['userDocument'] = $userDocument->where('user_id', $id)->find();
            $response['data']['userMatchDetails'] = $userMatchDetails->where('from_user_id', $id)->find();
            $response['data']['transactionDetails'] = $transactionDetails->where('user_id', $id)->find();
            $response['data']['userOrders'] = $userOrders->where('user_id', $id)->find();

            $sessionModel = new SessionModel();

            if($sessionModel->checkAdmin()){
                $response['data']['isAdmin'] = TRUE;
                if($sessionModel->checkProfileIdIdWithSession($profileId)){
                    $response['data']['isOwnProfile'] = TRUE;
                } else {
                    $response['data']['isOwnProfile'] = FALSE;
                }
            } else if($sessionModel->checkProfileIdIdWithSession($profileId)){
                $response['data']['isOwnProfile'] = TRUE;
                $response['data']['isAdmin'] = FALSE;
            } else {
                $response['data']['isAdmin'] = FALSE;
                $response['data']['isOwnProfile'] = FALSE;
            }
            $response['data']['profileId'] = $profileId;
            $response['status'] = true;
            $response['message'] = "Success";

            return $response;

        } else {
            return ["status"=>false,"message"=>"Profile Details not found","data"=>[]];
        }

    }

    public function upsertFamilyDetail($familyData){

        $userDetails = new UserDetails();
        $familyDetails = new FamilyDetails();
        
        //check user Details Table if user exists then update family details
        $userId = $userDetails->where('id', $familyData['user_id'])->findColumn('id');

        //return $userId;
        if($userId){

            $id = $familyDetails->where('user_id', $userId)->findColumn('id');
            
            if($id)
            {
                $result = db_connect()->table('family_details')->where('user_id',$userId)->update($familyData) ?["status"=>true,"message"=>"Family Details Updated","data"=>[]]:["status"=>false,"message"=>"Family Details Not Updated","data"=>[]];

            } else {
                                
                $result = db_connect()->table('family_details')->insert($familyData) > 0 ?["status"=>true,"message"=>"Family Details Updated","data"=>[]]:["status"=>false,"message"=>"Family Details Not Updated","data"=>[]];
                
            
            }

        } else {
            $result = ["status"=>false,"message"=>"User Not Registered.","data"=>[]];
        }

        return $result;
    }

    public function upsertDesirePatnerDetails($desireDetail){

        $userDetails = new UserDetails();
        $patnerExpectation = new PatnerExpectation();
        
        $userId = $userDetails->where('id', $desireDetail['user_id'])->findColumn('id');

        if($userId){

            $id = $patnerExpectation->where('user_id', $userId)->findColumn('id');
            
            if($id)
            {
                $result = db_connect()->table('patner_expectation')->where('user_id',$userId)->update($desireDetail) ?["status"=>true,"message"=>"Patner Details Updated","data"=>[]]:["status"=>false,"message"=>"Patner Details Not Updated","data"=>[]];

            } else {
                                
                $result = db_connect()->table('patner_expectation')->insert($desireDetail) > 0 ?["status"=>true,"message"=>"Patner Details Updated","data"=>[]]:["status"=>false,"message"=>"Patner Details Not Updated","data"=>[]];
                
            }

        } else {
            $result = ["status"=>false,"message"=>"User Not Registered.","data"=>[]];
        }

        return $result;
    }

    public function updateUserProfile($profileData){

        $userDetails = new UserDetails();
        
        $userId = $userDetails->where('id', $profileData['user_id'])->findColumn('id');

        if($userId){
        
            $result = db_connect()->table('user_profile_details')->where('user_id',$userId)->update($profileData) ?["status"=>true,"message"=>"Details Updated","data"=>[]]:["status"=>false,"message"=>"Details Not Updated","data"=>[]];

        } else {
    
            $result = ["status"=>false,"message"=>"User Not Registered.","data"=>[]];
    
        }

        return $result;
    }
}