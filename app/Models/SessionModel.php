<?php namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;

use CodeIgniter\Model;
use \Firebase\JWT\JWT;

class SessionModel extends Model
{
    
    public function __construct()
    {
        helper(['form', 'url']);

    }
    public function checkUserIdWithSession($user_id){
        $this->session = \Config\Services::session();
        $userId = $this->session->get('user_id');  
        return $user_id == $userId;
    }

    public function checkProfileIdIdWithSession($profile_id){
        $this->session = \Config\Services::session();
        $profileId = $this->session->get('profile_id');  
        return $profile_id == $profileId;
    }

    public function getUserIdFromSession(){
        $this->session = \Config\Services::session();
        $userId = $this->session->get('user_id');  
        return $userId;
    }

    public function getProfileIdFromSession(){
        $this->session = \Config\Services::session();
        $profile_id = $this->session->get('profile_id');  
        return $profile_id;
    }

            // $this->session->set('',$jwtToken);
        //         $this->session->set('user_id',$user_id);  
        //         $this->session->set('profile_id',$profile_id);  
        //         $this->session->set('user_role',$user_role);  

    public function validateToken(){
        $local_session = \Config\Services::session();
        $jwt = $local_session->get('token');  
        
        if($jwt){

            $key = "mirakleyabaze";
            try{
                $decoded = JWT::decode($jwt, $key, array('HS256'));
                return $decoded;
            } catch(Exception $e){
                $jwtToken = ["status"=>false,"message"=>"Authentication Failed","data"=>$e->getMessage() ];
                //return redirect('login');
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function checkAdmin(){
        $this->session = \Config\Services::session();
        $user_role = $this->session->get('user_role');  

        if($user_role == "ROLE_ADMIN") {
            return TRUE;
        } else{
            return FALSE;
        }
    }
}