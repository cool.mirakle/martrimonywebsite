<?php namespace App\Models;

use CodeIgniter\Model;

class UserOrders extends Model
{
    protected $table      = 'user_orders';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    //protected $useSoftDeletes = true;

    protected $allowedFields = ['user_id', 'orders_id','status','payment_mode','payment_gateway','email_id','mobile_no','user_sub_id'];

    protected $useTimestamps = true;
    //protected $createdField  = '';
    //protected $updatedField  = '';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    
}