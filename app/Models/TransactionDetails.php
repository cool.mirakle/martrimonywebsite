<?php namespace App\Models;

use CodeIgniter\Model;

class TransactionDetails extends Model
{
    protected $table      = 'transaction_dtls';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    //protected $useSoftDeletes = true;

    protected $allowedFields = ['user_id', 'order_id','status','inserted_date_time','modified_date_time','transaction_id'];

    protected $useTimestamps = true;
    //protected $createdField  = '';
    //protected $updatedField  = '';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    
}