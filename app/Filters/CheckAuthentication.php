<?php
// application/Filters/PostRequestOnly.php
declare(strict_types=1);
 
namespace App\Filters;
 
use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Config\Services;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\IncomingRequest;
use \Firebase\JWT\JWT;
use Exception;

final class CheckAuthentication implements FilterInterface
{
    public function before(RequestInterface $request,$arguments = NULL)
    {
        //if ($request->getHeaders()->== 'post') {

            $response = Services::response();

            $request1 = service('request');

            //print_r($request1);
            //print_r($request1->getHeader('X-API-Key'));
            $response->setHeader('Content-type', 'application/json');
            //$response->setHeader('X-API-Key', "asdfghjk");
    
            $response->noCache();
                
            $key = "mirakleyabaze";
            $jwt = $request1->getHeaderLine('X-API-Key');
            try{
                $decoded = JWT::decode($jwt, $key, array('HS256'));
                return;
            } catch(Exception $e){
                $jwtToken = $response->setBody(json_encode ( ["status"=>false,"message"=>"Authentication Failed","data"=>$e->getMessage() ] ));
                // return redirect('login');
                return $response
                   ->setStatusCode(ResponseInterface::HTTP_UNAUTHORIZED);
            }

            $response->send();

        }
 
    public function after(RequestInterface $request, ResponseInterface $response,$arguments = NULL)
    {

    }

}