<header>

    <div class="navbar navbar-inverse-blue navbar">
        <!--<div class="navbar navbar-inverse-blue navbar-fixed-top">-->
        <div class="navbar-inner">
            <div class="container">
                <div class="menu">
                    <div class="cd-dropdown-wrapper">
                        <div class="cd-dropdown-trigger">Thirupathy Yadava Matrimony</div>

                    </div> <!-- .cd-dropdown-wrapper -->
                </div>
                <div class="pull-right">
                    <nav class="navbar nav_bottom" role="navigation">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header nav_2">
                            <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse"
                                data-target="#bs-megadropdown-tabs">Menu
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                            <ul class="nav navbar-nav nav_1">
                                <li class="active"><a href="#">Home</a></li>
                                <li><a href="about">About</a></li>
                                <li><a href="profile">Profile</a></li>
                                <li class="last"><a href="logout">Logout</a></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </nav>
                </div> <!-- end pull-right -->
                <div class="clearfix"> </div>
            </div> <!-- end container -->
        </div> <!-- end navbar-inner -->
    </div> <!-- end navbar-inverse-blue -->
    <!-- ============================  Navigation End ============================ -->
</header>

<link rel='stylesheet' href='https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css'>

<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'>


<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-utils/0.1.1/angular-ui-utils.min.js"></script>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.0.0/ui-bootstrap-tpls.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>

<style type="text/css">
table tr th {
    background: #337ab7;
    color: white;
    text-align: left;
    vertical-align: center;
}
</style>

<div class="container" ng-app="formvalid">
    <div class="panel" data-ng-controller="validationCtrl" data-ng-init="init()">

        <div class="panel-body" ng-if="showDetails">
            <table #example class="table table-bordered bordered table-striped table-condensed datatable"
                ui-jq="dataTable" ui-options="dataTableOpt">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th class="nosort">Photo</th>
                        <th>Profile ID</th>
                        <th>Email</th>
                        <th>Name</th>
                        <th>Religion</th>
                        <th>Caste</th>
                        <th>Star</th>
                        <th>Rasi</th>
                        <th>DOB</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="n in data1">
                        <td>{{$index+1}}</td>
                        <td><img src="/images/profile-image-girl.jpg"
                            ng-src="/readFile?fileName={{n.profile_pic}}" alt="profile image" /></td>
                        <td><a href="profile?profileId={{n.profile_id}}">{{n.profile_id}}</td>
                        <td>{{n.user_name}}</td>
                        <td>{{n.first_name}} {{n.middle_name}} {{n.last_name}}</td>
                        <td>{{n.religion}}</td>
                        <td>{{n.caste}}</td>
                        <td>{{n.star}}</td>
                        <td>{{n.rasi}}</td>
                        <td>{{n.dob | date:'dd/MM/yyyy'}}</td>
                    </tr>
                </tbody>
            </table>
        </div>


        <div class="w3layous-story text-center" ng-hide="!showSubscription">
		<div class="container">
			<h4>Your story is waiting to happen! To Activate your account please puchase your plan.</h4>
			<h4><a href="plan">Purchase Plans</a></h4>
		</div>
        </div> 

        <div class="w3layous-story text-center" ng-hide="!showVerificationPending">
		<div class="container">
			<h4>Your story is waiting to happen! Payment Verification Pending</h4>
		</div>
        </div> 

        <div class="w3layous-story text-center" ng-hide="!showPaymentRejected">
		<div class="container">
			<h4>Your story is waiting to happen! To Happen Please Made Payment Again. Your Payment got rejected</h4>
			<h4><a href="plan">Purchase Plans</a></h4>
		</div>
        </div> 

    </div>
</div>



<?php
    $local_session = \Config\Services::session(); // Needed for Point 5
?>
<?= $local_session->get('user_id'); ?>

<script>
'use strict';

var app = angular.module('formvalid', ['ui.bootstrap', 'ui.utils']);
app.controller('validationCtrl', function($scope, $http, $window) {

    $http.post('/Admin/userDetails', {}, {
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(function(response) {
    
        //response.data.data.includes("world");
        var a = response.data.message;
        console.log(response);

        $scope.showSubscription = false;
        $scope.showDetails = false;
        $scope.showVerificationPending = false;
        $scope.showPaymentRejected = false;

        if(a.includes("Subscribe")){
            // show Plan Subscription
            //$window.location.href = '/Pages/view/plan';
            $scope.showSubscription = true;

        } else if(a.includes("Authentication")){
            // redirect to login
            $window.location.href = '/Pages/view/logout';

        } else if(a.includes("Verification")){
            $scope.showVerificationPending = true;
        } else if(a.includes("Rejected")){
            $scope.showPaymentRejected = true;
        } else {
            $scope.showDetails = true;
        }
        $scope.data1 = response.data.data;
        
        $scope.apply();
        $('#example').DataTable().draw();
    
    });

    $scope.showSubscription = false;

    $scope.data1;

    $scope.dataTableOpt = {
        "aLengthMenu": [
            [5, 10, 50, 100, -1],
            [5, 10, 50, 100, 'All']
        ],
    };
});
</script>