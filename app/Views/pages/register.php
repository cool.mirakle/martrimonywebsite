<div class="w3layouts-banner" id="home">
    <div class="container">
        <div class="logo">
            <h1><a class="cd-logo link link--takiri" href="index.php">Match <span><i class="fa fa-heart"
                            aria-hidden="true"></i>Made in heaven.</span></a></h1>
        </div>
        <div class="clearfix"></div>
        <div class="agileits-register">
            <h3>
                <center>Register NOW!</center>
            </h3>

            <div ng-app="registerApplication">
                <form name="login" ng-controller="registerController" ng-submit="registerSubmit()">

                    <div class="w3_modal_body_grid">
                        <span>Registration For *</span>
                        <select id="w3_country" name="register_for" ng-model="data.register_for"
                            class="frm-field required" required="">
                            <option value="">Select</option>
                            <option value="Myself">Myself</option>
                            <option value="Son">Son</option>
                            <option value="Daughter">Daughter</option>
                            <option value="Brother">Brother</option>
                            <option value="Sister">Sister</option>
                            <option value="Relative">Relative</option>
                            <option value="Friend">Friend</option>
                        </select>
                    </div>
                    <div class="w3_modal_body_grid w3_modal_body_grid1">
                        <span>First Name *</span>
                        <input type="text" name="firstName" ng-model="data.firstName" placeholder="First name"
                            required="" />
                    </div>
                    <div class="w3_modal_body_grid w3_modal_body_grid1">
                        <span>Middle Name </span>
                        <input type="text" name="middleName" ng-model="data.middleName" placeholder="Middle Name" />
                    </div>
                    <div class="w3_modal_body_grid w3_modal_body_grid1">
                        <span>Last Name</span>
                        <input type="text" name="lastName" ng-model="data.lastname" placeholder="Last Name" />
                    </div>
                    <div class="w3_modal_body_grid">
                        <span>Gender *</span>
                        <div class="w3_gender">
                            <div class="colr ert">
                                <label class="radio"><input type="radio" name="gender" ng-model="data.gender"
                                        value="Male" checked=""><i></i>Male</label>
                            </div>
                            <div class="colr">
                                <label class="radio"><input type="radio" name="gender" ng-model="data.gender"
                                        value="Female"><i></i>Female</label>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                        <div class="clearfix"> </div>
                    </div>



                    <div class="w3_modal_body_grid w3_modal_body_grid1">
                        <span>Date Of Birth:</span>
                    </div>


                    <div class="form-group">
                        <div class='input-group date' id='datepicker'>

                            <input type='text' class="form-control" name="dob" ng-model="data.dob"
                                placeholder="mm/dd/yyyy" required="" />
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>


                    <div class="w3_modal_body_grid">
                        <span>religion:</span>
                        <select id="w3_country1" name="religion" ng-model="data.religion" class="frm-field required"
                            required="">
                            <option value="">Select Religion</option>
                            <option value="Muslim">Muslim</option>
                            <option value="Hindu">Hindu</option>
                            <option value="Christian">Christian</option>
                            <option value="Sikh">Sikh</option>
                            <option value="Jain">Jain</option>
                            <option value="Buddhist">Buddhist</option>
                            <option value="No Religious Belief">No Religious Belief</option>
                        </select>
                    </div>
                    <div class="w3_modal_body_grid w3_modal_body_grid1">
                        <span>Mobile No *</span>
                        <!-- country codes (ISO 3166) and Dial codes. -->
                        <input id="phone" type="tel" name="phone_number" ng-model="data.phone_number" maxlength="10"
                            minlength="10" required="">
                        <!-- Load jQuery from CDN so can run demo immediately -->
                        <script src="/js/intlTelInput.js"></script>
                        <script>
                        $("#phone").intlTelInput({
                            // allowDropdown: false,
                            // autoHideDialCode: false,
                            // autoPlaceholder: "off",
                            // dropdownContainer: "body",
                            // excludeCountries: ["us"],
                            // formatOnDisplay: false,
                            // geoIpLookup: function(callback) {
                            //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
                            //     var countryCode = (resp && resp.country) ? resp.country : "";
                            //     callback(countryCode);
                            //   });
                            // },
                            //initialCountry: "auto",
                            // nationalMode: false,
                            onlyCountries: ['in'],
                            placeholderNumberType: "MOBILE",
                            // preferredCountries: ['cn', 'jp'],
                            // separateDialCode: true,
                            utilsScript: "/js/utils.js"
                        });
                        </script>
                    </div>

                    <div class="w3_modal_body_grid">
                        <span>Email *</span>
                        <input type="email" name="user_name" ng-model="data.user_name" placeholder="E-Mail *"
                            required="" />
                    </div>
                    <div class="w3_modal_body_grid w3_modal_body_grid1">
                        <span>Password *</span>
                        <input type="password" name="password" ng-model="data.password" placeholder="Password *"
                            required="" maxlength="20" minlength="6" />
                    </div>

                    <label class="file">
                        <input type='file' class="image-input" name="profile_pic" file-model="myFile" id="profile_pic"
                            required="" onchange="readURL(this);" />
                        <img id="blah" src="/images/profile_alt_image.png" alt="Profile Image" width="100"
                            height="100" />

                    </label>

                    <div class="w3-agree">
                        <input type="checkbox" id="c1" name="agreement" ng_model="data.agreement" required="">
                        <label class="agileits-agree">I have read & agree to the Terms and Conditions. </label>
                    </div>
                    <input type="submit" value="Register me" />
                    <div class="clearfix"></div>
                    <p class="w3ls-login">Already a member? <a href="login">Login</a>
                    </p>
                </form>
            </div>
        </div>

    </div>
</div>

<script>
var app = angular.module('registerApplication', []);

app.directive('fileModel', ['$parse', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function() {
                scope.$apply(function() {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.service('fileUpload', function($http, $window) {

    this.uploadFileToUrl = function(file, userName) {

        $http({
            method: 'POST',
            url: '/fileUpload',
            headers: {
                'Content-Type': undefined
            },
            data: {
                userfile: file,
                fileType: 1,
                "documentName": "Profile",
                "userName": userName
            },
            transformRequest: function(data, headersGetter) {
                var formData = new FormData();
                angular.forEach(data, function(value, key) {
                    formData.append(key, value);
                });
                return formData;
            }
        }).success(function(data, status, headers, config) {
            console.log(data, status, headers, config);

            if (data.status && status == 200) {
                alert(data.message);
            }
            $window.location.href = '/Pages/view/login';

        }).error(function(data, status, headers, config) {
            console.log(data, status, headers, config);
            $window.location.href = '/Pages/view/login';

        });

    }
});

app.service('register', ['fileUpload','$http', '$window', function(fileUpload, $http, $window) {

            this.registerUser = function(data, file) {

                $http.post('/api/register', {
                    "user_name": data.user_name,
                    "password": data.password,
                    "gender": data.gender,
                    "dob": data.dob,
                    "firstName": data.firstName,
                    "lastName": data.lastName,
                    "middleName": data.middleName,
                    "phone_number": data.phone_number,
                    "Religion": data.Religion,
                    "register_for": data.register_for,
                }).then(function(response) {

                    item = response.data;
                    alert(item.message);

                    if (item.status) {
                        fileUpload.uploadFileToUrl(file, data.user_name);
                    }

                });

            }
        }]);

        app.controller(
            'registerController', ['$scope', 'register',
                function($scope, register) {

                    $scope.registerSubmit = function() {

                        register.registerUser($scope.data, $scope.myFile);
                    }
                }
            ]);
</script>


<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script>
$(function() {
    $('#datepicker').datepicker({
        format: "mm/dd/yyyy",
        autoclose: true,
        todayHighlight: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        autoclose: true,
        changeMonth: true,
        changeYear: true,
        orientation: "button"
    });
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#blah')
                .attr('src', e.target.result)
                .width(150)
                .height(200);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>