<header>

    <div class="navbar navbar-inverse-blue navbar">
        <!--<div class="navbar navbar-inverse-blue navbar-fixed-top">-->
        <div class="navbar-inner">
            <div class="container">
                <div class="menu">
                    <div class="cd-dropdown-wrapper">
                        <div class="cd-dropdown-trigger">Thirupathy Yadava Matrimony</div>

                    </div> <!-- .cd-dropdown-wrapper -->
                </div>
                <div class="pull-right">
                    <nav class="navbar nav_bottom" role="navigation">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header nav_2">
                            <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse"
                                data-target="#bs-megadropdown-tabs">Menu
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                            <ul class="nav navbar-nav nav_1">
                                <li ><a href="dashboard">Home</a></li>
                                <li class="active"><a href="#">Plan</a></li>
                                <li><a href="about">About</a></li>
                                <li><a href="profile">Profile</a></li>
                                <li class="last"><a href="logout">Logout</a></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </nav>
                </div> <!-- end pull-right -->
                <div class="clearfix"> </div>
            </div> <!-- end container -->
        </div> <!-- end navbar-inner -->
    </div> <!-- end navbar-inverse-blue -->
    <!-- ============================  Navigation End ============================ -->
</header>

<link rel='stylesheet' href='https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css'>

<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-utils/0.1.1/angular-ui-utils.min.js"></script>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.0.0/ui-bootstrap-tpls.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>

<style type="text/css">
table tr th {
    background: #337ab7;
    color: white;
    text-align: left;
    vertical-align: center;
}
</style>

<div class="container" ng-app="plan">
    <div class="panel" data-ng-controller="planDetailsController">

        <!--inner block start here-->
        <div class="inner-block">
            <div class="price-block-main">
                <div class="prices-head">
                    <h2>Plan Details</h2>
                </div>
                <div class="price-tables">
                    <!-- ng-if="planDetails.length > 0" -->

                    <div class="col-md-4 price-grid" ng-repeat="plan in planDetails">

                        <div class="price-block">
                            <div class="price-gd-top pric-clr{{plan.id%3+1}}">
                                <h4>{{plan.plan_name}}</h4>
                                <h3><span class="usa-dollar">Rs</span> {{plan.price}}</h3>
                                <h5>Full Package</h5>
                            </div>
                            <div class="price-gd-bottom">
                                <div class="price-list">
                                    <ul>
                                        <li>{{plan.description}}</li>

                                    </ul>
                                </div>
                            </div>
                            <div class="price-selet pric-clr{{plan.id%3+1}}">
                                <a class="popup-with-zoom-anim" href="#small-dialog" ng-click="selectPlan(plan)" href="#" data-toggle="modal" data-target="#paymentUpload">Select
                                    Plan</a>
                                    
                            </div>

                        </div>
                    </div>

                </div>

                <div ng-if="planDetails.length <= 0">
                    <div class="form_but1">
                        <div class="col-sm-9 w3_details">
                            Plan Details Not Available.
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>



                <div class="clearfix"> </div>
            </div>
        </div>


        <div id="paymentUpload" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Payment</h4>
                    </div>
                    <div class="modal-body">
                        <div class="login-w3ls">

                            <img src="/images/profile-image-girl.jpg"
                            ng-src="/readFile?fileName=yabaze_phonepe.png" alt="profile image" />
                 
                            <input type="text" name="receiptRefNumber" ng-model="receiptRefNumber" placeholder="Enter Payment Reference Number">
                            <input type="text" name="Paid Amount" ng-model="paidAmount" placeholder="Enter Payment Amount">

                            <!-- <label class="file">
                            Please Upload Recipt
                            <input type='file' class="image-input" name="document" file-model=""
                                id="document" required="" onchange="readURL(this);"/>
                                <br>
                            
                            </label> -->

                            <label class="file">
                            <input type='file' class="image-input" name="profile_pic" file-model="paymentFile"
                                id="profile_pic" required="" onchange="readURL(this);" />
                                <br>
                            <img id="blah" src="/images/profile_alt_image.png" alt="Profile Image" width="100"
                                height="100" />

                            </label>

                    

                            <br>
                            <button ng-click="uploadPayment()">Upload Payment</button>
                            <div class="clearfix"> </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //Modal -->

    </div>
    <!--inner block end here-->

</div>
</div>

<script>
function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<script>
'use strict';

var app = angular.module('plan', []);
app.directive('fileModel', ['$parse', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function() {
                scope.$apply(function() {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.controller('planDetailsController', function($scope, $http,$window) {

    $scope.planDetails;
    $scope.selectedPlan;
    $http.post('/Admin/getPlanDetails', {}, {
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(function(response) {

        if (!response.data.status) {
            alert("Plan Details Not Found");
        }

        $scope.planDetails = response.data.data.planDetails;
        console.log(response);

    });

    $scope.uploadPayment = function() {

        console.log("Upload Payment");

        var paymentReference = $scope.receiptRefNumber;
        var refFile  =  $scope.paymentFile;
        var paidAmount = $scope.paidAmount;

        if(paidAmount==null && paidAmount == undefined){
            alert("Please Enter a Payment Amount")
            return
        }

        if(paymentReference==null && paymentReference == undefined){
            alert("Please Enter a reference Number")
            return
        }

        $http({
            method: 'POST',
            url: '/paymentUpload',
            headers: {
                'Content-Type': undefined
            },
            data: {
                "paymentFile": $scope.paymentFile,
                "reference": paymentReference,
                "planDetails": $scope.selectedPlan,
                "paidAmount": $scope.paidAmount,
                "planId": $scope.selectedPlan.id,
                "planAmount": $scope.selectedPlan.price,
                "planName": $scope.selectedPlan.plan_name
            },
            transformRequest: function(data, headersGetter) {
                var formData = new FormData();
                angular.forEach(data, function(value, key) {
                    formData.append(key, value);
                });
                return formData;
            }
        }).success(function(data, status, headers, config) {
            console.log(data, status, headers, config);

            if (data.status && status == 200) {
                 alert(data.message);
                 $('#paymentUpload').modal('hide');
                 $window.location.href = '/Pages/view/dashboard';
             } else {
                alert(data.message);
             }

        }).error(function(data, status, headers, config) {
            console.log(data, status, headers, config);
            alert("Failed To upload Payment");
            //$window.location.href = '/Pages/view/login';
        });
        
    }

    $scope.selectPlan = function(plan) {
        $scope.selectedPlan = plan;
    }

});
</script>
<script src="/js/jquery.magnific-popup.js" type="text/javascript"></script>
<script>
$(document).ready(function() {
    $('.popup-with-zoom-anim').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in'
    });

});
</script>
<link href="/css/demo-page.css" rel="stylesheet" type="text/css" media="all" />