<div class="w3layouts-banner" id="home" ng-app="loginApplication" ng-controller="loginController">
    <div class="container">
        <div class="logo">
            <h1><a class="cd-logo link link--takiri" href="index.php">Match <span><i class="fa fa-heart"
                            aria-hidden="true"></i>Made in heaven.</span></a></h1>
        </div>
        <div class="clearfix"></div>
        <div class="agileits-register">

            <h3>
                <center>Login</center>
            </h3>

            <!-- <div ng-app="loginApplication" ng-controller="loginController"> -->
            <form name="login" ng-submit="submitForm()">


                <div class="w3_modal_body_grid">
                    <span>User Name</span>
                    <input type="email" name="username" ng-model="loginData.username" placeholder="Username"
                        required="" />
                </div>
                <div class="w3_modal_body_grid w3_modal_body_grid1">
                    <span>Password</span>
                    <input type="password" name="password" placeholder="Password" ng-model="loginData.password"
                        required="" maxlength="20" minlength="6" />
                </div>

                <div class="w3-agree">
                    <label class="agileits-agree"> <a href="#" data-toggle="modal" data-target="#forgotPassword">Forgot
                            Password?</a></label>
                </div>

                <!-- <input type="submit" /> -->
                <input type="submit" value="Login" />

                <div class="clearfix"></div>
                <p class="w3ls-login">Don't Have An Account? <a href="register">Register Here</a></p>

            </form>

            <!-- </div> -->

        </div>
        <!-- Modal -->
        <div id="forgotPassword" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Forgot Password</h4>
                    </div>
                    <div class="modal-body">
                        <div class="login-w3ls">

                            <label>User Name </label>
                            <!-- <input type="text" name="username" placeholder="Username" required=""> -->
                            <input type="text" name="username" ng-model="username" placeholder="Username" required="" />
                            <div class="clearfix"> </div>
                            <br>
                            <button ng-click="forgotPassword()">Forgot password</button>
                            <div class="clearfix"> </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //Modal -->
    </div>
</div>

<script>
var app = angular.module('loginApplication', []);
app.controller('loginController', function($scope, $http, $window) {

    $http.get('/api/logout').then(function(response) {

            console.log(response);
            alert(response.data.message);

    });

    $scope.submitForm = function() {

        $http.post('/api/login', {
            "username": $scope.loginData.username,
            "password": $scope.loginData.password //Nv6gjnoyjxAs
        }).then(function(response) {

            console.log(response);
            alert(response.data.message);

            if (response.data.status) {
                $window.location.href = '/Pages/view/dashboard';
                $scope.loginData.username = '';
                $scope.loginData.password = '';
            } else {

            }

        });

    }

    $scope.forgotPassword = function() {

        console.log("forgot Password");

        $http.post('/api/forgotPassword', {
            "username": $scope.username,
        }).then(function(response) {

            if (response.data.status) {
                $('#forgotPassword').modal('hide');
                alert(response.data.message);
                //$window.location.href = '/Pages/view/home';
            } else {
                alert(response.data.message);
            }

            //});

        });
        return false;
    }

});
</script>