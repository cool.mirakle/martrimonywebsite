<div ng-app="profilePage" ng-controller="profileController">

    <div class="w3layouts-inner-banner" ng-init="profileId='<?= $profileId ?>'">
        <div class="container">
            <div class="logo">
                <h1><a class="cd-logo link link--takiri" href="home">Match <span><i class="fa fa-heart"
                                aria-hidden="true"></i>Made in heaven.</span></a></h1>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- //inner banner -->

    <!-- breadcrumbs -->
    <div class="w3layouts-breadcrumbs text-center">
        <div class="container">
            <span class="agile-breadcrumbs"><a href="dashboard">Home</a> >
                <span>
                    <!-- < ?= $profileId ?> -->
                    {{profileId}}
                </span></span>
        </div>
    </div>
    <!-- //breadcrumbs -->

    <!-- Bride Profile Details -->
    <div class="w3ls-list">
        <div class="container">
            <h2>Profile Details</h2>
            <div class="col-md-9 profiles-list-agileits">
                <div class="single_w3_profile">
                    <div class="agileits_profile_image">
                        <img src="/images/profile-image-girl.jpg"
                            ng-src="/readFile?fileName={{profileDetails.profile_pic}}" alt="profile image" />
                    </div>
                    <div class="w3layouts_details">
                        <h4>Profile ID {{userDetails.profile_id}}
                        </h4>
                        <span>Last Active on
                            {{userDetails.last_active_time}}
                        </span>
                        <p>
                            <!-- {{profileDetails.age}} -->
                            <!-- < ? php print_r($profile_details->{'age'}.'/'.$profile_details->{'height'}.'/'.$profile_details->{'weight'}.'/'.$profile_details->{'gender'}.','.$profile_details->{'religion'}.','.$profile_details->{'caste'}.','.$profile_details->{'language'});?> -->
                        </p>

                        <p>Status<span>&nbsp {{activeStatus}}

                            </span></p>

                        <!-- < ?php if($_SESSION['user_role']== 'ROLE_ADMIN') {?> -->
                        <a href="#" data-toggle="modal" data-target="#updateProfileStatus"
                            ng-if="isAdmin||isOwnProfile">Update Profile Status</a>

                        <a href="#" data-toggle="modal" data-target="#updateProfilePhoto"
                            ng-if="isAdmin||isOwnProfile">Update Profile Photo</a>

                        <a href="#" data-toggle="modal" data-target="#resetProfilePassword" ng-if="isAdmin">Reset
                            Profile Password</a>

                        <a href="#" data-toggle="modal" data-target="#updatePayment" ng-if="isAdmin">Update Payment
                            Information</a>

                        <a href="#" data-toggle="modal" data-target="#changePassword" ng-if="isOwnProfile">Change
                            Password</a>

                    </div>
                    <div class="clearfix"></div>
                </div>




                <div class="profile_w3layouts_details">
                    <div class="agileits_aboutme">










                        <h4>About me</h4>
                        <h5>Brief about me <a class="agile-icon" href="#" data-toggle="modal"
                                data-target="#aboutMeEditModel" ng-if="isAdmin||isOwnProfile">
                                <i class="fa fa-edit"></i> Edit
                            </a></h5>

                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">
                                Name
                            </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.first_name}} {{profileDetails.middle_name}}
                                {{profileDetails.last_name}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">
                                Gender
                            </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.gender}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">
                                Date Of Birth & Age
                            </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.dob}} & {{profileDetails.age}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">
                                Blood Group
                            </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.blood_group}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">
                                Religion
                            </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.religion}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>


                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">
                                Star
                            </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.star}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">
                                Rasi
                            </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.rasi}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">
                                Caste & SubCaste
                            </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.caste}} & {{profileDetails.subcaste}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">
                                Qualification
                            </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.education}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>


                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">
                                Occupation
                            </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.occupation}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>


                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">
                                Salary
                            </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.salary}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">
                                Contact
                            </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.phone_no}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">
                                E-mail
                            </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.email_id}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">
                                State
                            </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.state}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">
                                Nationality
                            </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.nationality}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">
                                Address
                            </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.address}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">
                                Personal Description
                            </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.profile_description}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                        <h5>SubScription Details </h5>

                        <div ng-if="planDetails">

                            <div class="form_but1">
                                <label class="col-sm-3 control-label1">
                                    Plan
                                </label>
                                <div class="col-sm-9 w3_details">
                                    {{planDetails.plan_name}}
                                </div>
                                <div class="clearfix"> </div>
                            </div>

                            <div class="form_but1">
                                <label class="col-sm-3 control-label1">
                                    Plan Price
                                </label>
                                <div class="col-sm-9 w3_details">
                                    {{planDetails.price}}
                                </div>
                                <div class="clearfix"> </div>
                            </div>

                            <div class="form_but1">
                                <label class="col-sm-3 control-label1">
                                    Plan Description
                                </label>
                                <div class="col-sm-9 w3_details">
                                    {{planDetails.description}}
                                </div>
                                <div class="clearfix"> </div>
                            </div>

                        </div>
                        <div ng-if="!planDetails">
                            <div class="form_but1">
                                <div class="col-sm-9 w3_details">
                                    Plan Not Subscribed Yet
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>

                        <h5>Family Details <a class="agile-icon" href="#" data-toggle="modal"
                                data-target="#familyEditModel" ng-if="isAdmin||isOwnProfile">
                                <i class="fa fa-edit"></i> Edit
                            </a></h5>

                        <div ng-if="familyDetails">

                            <div class="form_but1">
                                <label class="col-sm-3 control-label1">Father's Name </label>
                                <div class="col-sm-9 w3_details">
                                    {{familyDetails.father_name}}
                                </div>
                                <div class="clearfix"> </div>
                            </div>

                            <div class="form_but1">
                                <label class="col-sm-3 control-label1">
                                    Father's Occupation
                                </label>
                                <div class="col-sm-9 w3_details">
                                    {{familyDetails.father_occupation}}
                                </div>
                                <div class="clearfix"> </div>
                            </div>

                            <div class="form_but1">
                                <label class="col-sm-3 control-label1">Mother's Name </label>
                                <div class="col-sm-9 w3_details">
                                    {{familyDetails.mother_name}}

                                </div>
                                <div class="clearfix"> </div>
                            </div>

                            <div class="form_but1">
                                <label class="col-sm-3 control-label1">Mother's Occupation </label>
                                <div class="col-sm-9 w3_details">
                                    {{familyDetails.mother_occupation}}

                                </div>
                                <div class="clearfix"> </div>
                            </div>

                            <div class="form_but1">
                                <label class="col-sm-3 control-label1">Family Income </label>
                                <div class="col-sm-9 w3_details">
                                    {{familyDetails.family_income}}

                                </div>
                                <div class="clearfix"> </div>
                            </div>

                            <div class="form_but1">
                                <label class="col-sm-3 control-label1">Other Details </label>
                                <div class="col-sm-9 w3_details">
                                    {{familyDetails.family_description}}

                                </div>
                                <div class="clearfix"> </div>
                            </div>


                        </div>
                        <div ng-if="!familyDetails">
                            <div class="form_but1">
                                <div class="col-sm-9 w3_details">
                                    No Data Found
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>







                        <h5>Lifestyle <a class="agile-icon" href="#" data-toggle="modal"
                                data-target="#lifeStyleEditModel" ng-if="isAdmin||isOwnProfile">

                                <i class="fa fa-edit"></i> Edit
                            </a></h5>

                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">Height & Weight </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.height}} & {{profileDetails.weight}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">Own House </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.own_house}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">Foodies </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.foodies}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">Hobbies </label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.hobbies}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">Drinks & Level</label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.drinker}} & {{profileDetails.drinking_level}}
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                        <div class="form_but1">
                            <label class="col-sm-3 control-label1">Somking & Level</label>
                            <div class="col-sm-9 w3_details">
                                {{profileDetails.smoker}} & {{profileDetails.smoking_level}}

                            </div>
                            <div class="clearfix"> </div>
                        </div>



                        <h5>Desired Partner <a class="agile-icon" href="#" data-toggle="modal"
                                data-target="#userDesireEditModel" ng-if="isAdmin||isOwnProfile">
                                <i class="fa fa-edit"></i> Edit
                            </a></h5>


                        <div ng-if="patnerExpectation">
                            <div class="form_but1">
                                <label class="col-sm-3 control-label1">Age </label>
                                <div class="col-sm-9 w3_details">
                                    {{patnerExpectation.age_from}} - {{patnerExpectation.age_to}}
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="form_but1">
                                <label class="col-sm-3 control-label1">Height </label>
                                <div class="col-sm-9 w3_details">
                                    {{patnerExpectation.height_from}} - {{patnerExpectation.height_to}}
                                </div>
                                <div class="clearfix"> </div>
                            </div>


                            <div class="form_but1">
                                <label class="col-sm-3 control-label1">Marital Status </label>
                                <div class="col-sm-9 w3_details">
                                    {{patnerExpectation.marital_status}}
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="form_but1">
                                <label class="col-sm-3 control-label1">Religion </label>
                                <div class="col-sm-9 w3_details">
                                    {{patnerExpectation.religion}}
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="form_but1">
                                <label class="col-sm-3 control-label1">Caste </label>
                                <div class="col-sm-9 w3_details">
                                    {{patnerExpectation.caste}}
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="form_but1">
                                <label class="col-sm-3 control-label1">Education </label>
                                <div class="col-sm-9 w3_details">
                                    {{patnerExpectation.education}}
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="form_but1">
                                <label class="col-sm-3 control-label1">Employment Status </label>
                                <div class="col-sm-9 w3_details">
                                    {{patnerExpectation.employment_status}}
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="form_but1">
                                <label class="col-sm-3 control-label1">Income </label>
                                <div class="col-sm-9 w3_details">
                                    {{patnerExpectation.income_from}} - {{patnerExpectation.income_to}}
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="form_but1">
                                <label class="col-sm-3 control-label1">Description </label>
                                <div class="col-sm-9 w3_details">
                                    {{patnerExpectation.description}}
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>

                        <div ng-if="!patnerExpectation">
                            <div class="form_but1">
                                <div class="col-sm-9 w3_details">
                                    No Data Found
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>


                        <h5>User Documents <a class="agile-icon" href="#" data-toggle="modal"
                                data-target="#uploadDocument" ng-if="isAdmin||isOwnProfile">
                                <i class="fa fa-edit"></i> Add
                            </a></h5>


                        <div ng-if="userDocument.length > 0">

                            <div ng-repeat="doc in userDocument">
                                <div class="form_but1">
                                    <label class="col-sm-3 control-label1">
                                        <image ng-src="/readFile?fileName={{doc.doc_name}}" />
                                    </label>
                                    <div class="col-sm-9 w3_details">
                                        {{doc.type}}
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>



                            </div>

                        </div>

                        <div ng-if="userDocument.length <= 0">
                            <div class="form_but1">
                                <div class="col-sm-9 w3_details">
                                    No Document Found
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>









                    </div>
                </div>


            </div>

        </div>
        <div class="clearfix"></div>
    </div>

    <!-- Brief About Me -->

    <div id="aboutMeEditModel" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">About Me</h4>
                </div>
                <div class="modal-body">
                    <div class="login-w3ls">

                        <div class="w3_modal_body_grid w3_modal_body_grid1">
                            <span>First Name *</span>
                            <input type="text" name="first_name" ng-model="profile.first_name" placeholder="First Name"
                                required="">
                        </div>


                        <div class="w3_modal_body_grid w3_modal_body_grid1">
                            <span>Middle Name</span>
                            <input type="text" name="middle_name" ng-model="profile.middle_name"
                                placeholder="Middle Name">
                        </div>

                        <div class="w3_modal_body_grid w3_modal_body_grid1">
                            <span>Last Name</span>

                            <input type="text" name="last_name" ng-model="profile.last_name" placeholder="Last Name">
                        </div>

                        <div class="w3_modal_body_grid w3_modal_body_grid1">
                            <span>Gender</span>
                        </div>

                        <select id="w3_country" name="gender" ng-model="profile.gender" class="frm-field required"
                            required="">
                            <option value="">Select</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>

                        <div class="w3_modal_body_grid w3_modal_body_grid1">
                            <span>Date Of Birth</span>
                        </div>

                        <div class="form-group">
                            <div class='input-group date' id='datepicker'>

                                <input type='text' class="form-control" name="dob" ng-model="profile.dob"
                                    placeholder="mm/dd/yyyy" required="" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>

                        <div class="w3_modal_body_grid w3_modal_body_grid1">
                            <span>Blood Group</span>
                        </div>

                        <select id="w3_country" name="blood_group" ng-model="profile.blood_group"
                            class="frm-field required" required="">
                            <option value="">Select</option>
                            <option value="A+">A+</option>

                            <option value="A-">A-</option>
                            <option value="B+">B+</option>
                            <option value="B-">B-</option>

                            <option value="AB+">AB+</option>
                            <option value="AB-">AB-</option>

                            <option value="O+">O+</option>
                            <option value="O-">O-</option>

                        </select>


                        <div class="w3_modal_body_grid w3_modal_body_grid1">
                            <span>Religion</span>
                        </div>

                        <select id="w3_country" name="religion" ng-model="profile.religion" class="frm-field required"
                            required="">
                            <option value="">Select</option>
                            <option value="Christian">Christian</option>
                            <option value="Muslim">Muslim</option>
                            <option value="Hindu">Hindu</option>
                            <option value="No Religion">No Religion</option>
                        </select>


                        <div class="w3_modal_body_grid w3_modal_body_grid1">
                            <span>Star</span>
                        </div>

                        <select id="w3_country" name="star" ng-model="profile.star" class="frm-field required"
                            required="">
                            <option value="">Select Star</option>
                            <option value="abcd">abcd</option>
                            <option value="efgh">efgh</option>
                        </select>

                        <div class="w3_modal_body_grid w3_modal_body_grid1">
                            <span>Rasi</span>
                        </div>

                        <select id="w3_country" name="rasi" ng-model="profile.rasi" class="frm-field required">
                            <option value="">Select Rasi</option>
                            <option value="abcd">ajsdkdj</option>
                            <option value="efgh">asdklas</option>
                        </select>

                        <div class="w3_modal_body_grid w3_modal_body_grid1">
                            <span>Caste</span>
                        </div>

                        <select id="w3_country" name="caste" ng-model="profile.caste" class="frm-field required"
                            required="">
                            <option value="">Select</option>
                            <option value="abcd">abcd</option>
                            <option value="efgh">efgh</option>
                        </select>


                        <div class="w3_modal_body_grid w3_modal_body_grid1">
                            <span>SubCaste</span>
                        </div>

                        <select id="w3_country" name="subcaste" ng-model="profile.subcaste" class="frm-field required">
                            <option value="">Select</option>
                            <option value="abcd">ajsdkdj</option>
                            <option value="efgh">asdklas</option>
                        </select>


                        <div class="w3_modal_body_grid w3_modal_body_grid1">
                            <span>Qualification</span>

                            <input type="text" name="education" ng-model="profile.education"
                                placeholder="Qualification">
                        </div>


                        <div class="w3_modal_body_grid w3_modal_body_grid1">
                            <span>Occupation</span>

                            <input type="text" name="occupation" ng-model="profile.occupation" placeholder="Occupation">
                        </div>


                        <div class="w3_modal_body_grid w3_modal_body_grid1">
                            <span>Salary</span>

                            <input type="text" name="salary" ng-model="profile.salary" placeholder="Salary">
                        </div>

                        <div class="w3_modal_body_grid w3_modal_body_grid1">
                            <span>Email</span>

                            <input type="text" name="email_id" ng-model="profile.email_id" placeholder="Email">
                        </div>

                        <div class="w3_modal_body_grid w3_modal_body_grid1">
                            <span>Phone Number</span>

                            <input type="text" name="phone_no" ng-model="profile.phone_no" placeholder="Phone Number"
                                maxlength="10" minlength="10">
                        </div>

                        <div class="w3_modal_body_grid w3_modal_body_grid1">
                            <span>Address</span>

                            <input type="text" name="address" ng-model="profile.address" placeholder="Address">
                        </div>

                        <div class="w3_modal_body_grid w3_modal_body_grid1">
                            <span>Personal Description</span>

                            <input type="text" name="profile_description" ng-model="profile.profile_description"
                                placeholder="Personal Description">
                        </div>

                        <br>
                        <br>

                        <button ng-click="updateAboutMe()"> Update Profile </button>

                        <div class="clearfix"> </div>

                        <div class="clearfix"> </div>

                    </div>
                </div>
            </div>

        </div>
    </div>


    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js">
    </script>
    <script>
    $(function() {
        $('#datepicker').datepicker({
            format: "mm/dd/yyyy",
            autoclose: true,
            todayHighlight: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            autoclose: true,
            changeMonth: true,
            changeYear: true,
            orientation: "button"
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    </script>

    <!-- Brief About Me -->

    <!-- Family Details Update -->

    <div id="familyEditModel" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Family Details</h4>
                </div>
                <div class="modal-body">
                    <div class="login-w3ls">

                        <label>Father's Name </label>

                        <input type="text" name="father_name" ng-model="father_name" placeholder="Father's Name"
                            required="">

                        <label>Father's Occupation</label>

                        <input type="text" name="father_occupation" ng-model="father_occupation"
                            placeholder="Father's Occupation">

                        <label>Mother's Name </label>

                        <input type="text" name="mother_name" ng-model="mother_name" placeholder="Mother's Name"
                            required="">

                        <label>Father's Occupation</label>

                        <input type="text" name="mother_occupation" ng-model="mother_occupation"
                            placeholder="Mother's Occupation">

                        <label>Family Income</label>

                        <input type="text" name="family_income" ng-model="family_income"
                            placeholder="Family Total Income">

                        <label>Other Details</label>

                        <input type="text" name="family_description" ng-model="family_description"
                            placeholder="Description">

                        <div class="clearfix"> </div>
                        <br>

                        <button ng-click="updateFamilyDetails()">Update Family Details</button>
                        <div class="clearfix"> </div>

                        <div class="clearfix"> </div>


                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Family Details Update -->

    <!-- Life Style -->

    <div id="lifeStyleEditModel" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Lifestyle Details</h4>
                </div>
                <div class="modal-body">
                    <div class="login-w3ls">

                        <label>Height (in cm)</label>

                        <input type="text" name="height" ng-model="style.height" placeholder="Height in cm">

                        <label>Weight (in KG) </label>

                        <input type="text" name="weight" ng-model="style.weight" placeholder="Weight in KG" required="">

                        <label>Hobbies </label>

                        <input type="text" name="hobbies" ng-model="style.hobbies" placeholder="Hobbies Description">

                        <label>House</label>

                        <select id="w3_country" name="own_house" ng-model="style.own_house" class="frm-field required"
                            required="">
                            <option value="">Select</option>
                            <option value="Own House">Own House</option>
                            <option value="Rent">Rent</option>
                            <option value="Contract">Contract</option>
                        </select>

                        <label>Foodies</label>

                        <select id="w3_country" name="foodies" ng-model="style.foodies" class="frm-field required"
                            required="">
                            <option value="">Select</option>
                            <option value="Veg">Veg</option>
                            <option value="Non-Veg">Non-Veg</option>
                        </select>


                        <label>Smoker Details</label>

                        <select id="w3_country" name="smoker" ng-model="style.smoker" class="frm-field required"
                            required="">
                            <option value="">Select</option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>

                        <label>Smoker Level (Select If You Are Smoker)</label>

                        <select id="w3_country" name="smoking_level" ng-model="style.smoking_level"
                            class="frm-field required">
                            <option value="">Select Number Of cigarette</option>
                            <option value="Less Than 2">Less Than 2</option>
                            <option value="Less Than 10">Less Than 10</option>
                            <option value="More Than 10">More Than 10</option>
                        </select>

                        <label>Drunker Details</label>

                        <select id="w3_country" name="drinker" ng-model="style.drinker" class="frm-field required"
                            required="">
                            <option value="">Select</option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>

                        <label>Drunker Level (Select If You Are Drunker)</label>

                        <select id="w3_country" name="drinking_level" ng-model="style.drinking_level"
                            class="frm-field required">
                            <option value="">Select</option>
                            <option value="Daily">Daily</option>
                            <option value="Weekly Once">Weekly once</option>
                            <option value="Monthly Once">Monthly Once</option>
                            <option value="At Festival">At Festival</option>
                        </select>

                        <br>
                        <br>

                        <button ng-click="updateLifeStyle()"> Update </button>

                        <div class="clearfix"> </div>

                        <div class="clearfix"> </div>


                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Life Style -->

    <!-- Desired Patner -->

    <div id="userDesireEditModel" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Desired Patner Details</h4>
                </div>
                <div class="modal-body">
                    <div class="login-w3ls">

                        <label>Height From (in cm)</label>

                        <input type="number" name="height_from" ng-model="desire.height_from"
                            placeholder="Height From in cm">

                        <label>Height To (in cm)</label>

                        <input type="number" name="height_to" ng-model="desire.height_to" placeholder="Height To in cm">

                        <label>Age From </label>

                        <input type="number" name="age_from" ng-model="desire.age_from" placeholder="Age From"
                            required="">

                        <label>Age To </label>

                        <input type="number" name="age_to" ng-model="desire.age_to" placeholder="Age To" required="">

                        <label>Religion</label>

                        <select id="w3_country" name="religion" ng-model="desire.religion" class="frm-field required"
                            required="">
                            <option value="">Select</option>
                            <option value="Christian">Christian</option>
                            <option value="Muslim">Muslim</option>
                            <option value="Hindu">Hindu</option>
                            <option value="No Religion">No Religion</option>
                        </select>

                        <label>Caste</label>

                        <select id="w3_country" name="caste" ng-model="desire.caste" class="frm-field required"
                            required="">
                            <option value="">Select</option>
                            <option value="abcd">abcd</option>
                            <option value="efgh">efgh</option>

                        </select>

                        <label>Education Details</label>

                        <input type="text" name="education" ng-model="desire.education" placeholder="Education Details">

                        <label>Employment Status</label>

                        <select id="w3_country" name="employment_status" ng-model="desire.employment_status"
                            class="frm-field required" required="">
                            <option value="">Select</option>
                            <option value="Un Employed">Un Employed</option>
                            <option value="Government Employee">Government Employee</option>
                            <option value="Private Employee">Private Employee</option>
                            <option value="Daily Wage Employee">Daily Wage Employee</option>
                            <option value="Owner">Owner</option>
                        </select>

                        <label>Marital Status</label>

                        <select id="w3_country" name="marital_status" ng-model="desire.marital_status"
                            class="frm-field required" required="">
                            <option value="">Select</option>
                            <option value="UnMarried">UnMarried</option>
                            <option value="Divorced">Divorced</option>
                            <option value="Divorced">Widowed</option>
                        </select>

                        <label>Earning Money</label>

                        <select id="w3_country" name="income_status" ng-model="desire.income_status"
                            class="frm-field required">
                            <option value="">Select</option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>

                        <label>Income Expected From (Rs) </label>

                        <input type="number" name="income_from" ng-model="desire.income_from"
                            placeholder="Income Expected From">

                        <label>Income Expected To (Rs) </label>

                        <input type="number" name="income_to" ng-model="desire.income_to"
                            placeholder="Income Expected To">

                        <label>Other Expectation </label>

                        <input type="text" name="description" ng-model="desire.description"
                            placeholder="Other Expectation">

                        <br>
                        <br>

                        <button ng-click="updateDesirePatner()">Update</button>
                        <div class="clearfix"> </div>

                        <div class="clearfix"> </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Desired Partner -->

    <!-- Change Password -->

    <div id="changePassword" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change Password</h4>
                </div>
                <div class="modal-body">
                    <div class="login-w3ls">
                        <form id="savelifestyle">

                            <label>Enter Old Password</label>

                            <input type="text" name="oldPassword" ng-model="oldPassword" placeholder="Old Password">

                            <label>Enter New Password</label>

                            <input type="text" name="newPassword" ng-model="newPassword" placeholder="New Password">

                            <input type="hidden" name="user_id" />

                            <br>
                            <br>
                            <button ng-click="changePassword()">Change password</button>

                            <!-- <input type="submit" name="submit"> -->

                            <div class="clearfix"> </div>

                            <div class="clearfix"> </div>


                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Change Password -->

    <!-- Reset Password -->

    <div id="resetProfilePassword" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Reset Password</h4>
                </div>
                <div class="modal-body">
                    <div class="login-w3ls">
                        <form id="savelifestyle">

                            <label>Enter New Password</label>

                            <input type="text" name="newPassword" ng-model="newPassword" placeholder="New Password">

                            <input type="hidden" name="user_id" />

                            <br>
                            <br>
                            <button ng-click="resetPassword()">Reset password</button>

                            <div class="clearfix"> </div>

                            <div class="clearfix"> </div>


                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Reset Password -->


    <!-- Reset Password -->

    <div id="updatePayment" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Payment Information</h4>
                </div>
                <div class="modal-body">
                    <div class="login-w3ls">
                        <form id="savelifestyle">

                            <div ng-if="planDetails">

                                <div class="form_but1">
                                    <label class="col-sm-3 control-label1">
                                        Plan
                                    </label>
                                    <div class="col-sm-9 w3_details">
                                        {{planDetails.plan_name}}
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>

                                <div class="form_but1">
                                    <label class="col-sm-3 control-label1">
                                        Plan Price
                                    </label>
                                    <div class="col-sm-9 w3_details">
                                        {{planDetails.price}}
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>

                                <div class="form_but1">
                                    <label class="col-sm-3 control-label1">
                                        Plan Description
                                    </label>
                                    <div class="col-sm-9 w3_details">
                                        {{planDetails.description}}
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>

                            </div>
                            <div ng-if="!planDetails">
                                <div class="form_but1">
                                    <div class="col-sm-9 w3_details">
                                        Plan Not Subscribed Yet
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>

                        <img src="/images/profile-image-girl.jpg"
                            ng-src="/readFile?fileName={{userSubscription.refImage}}" alt="Payment Receipt" />

                            <br>
                            <label>Payment Status</label>

                            <select id="w3_country" name="employment_status" ng-model="paymentApprovalStatus"
                                class="frm-field required" required="">
                                <option value="">Select</option>
                                <option value="1">Verification Pending</option>
                                <option value="2">Verification Completed</option>
                                <option value="3">Payment Rejected</option>
                            </select>
                            <button ng-click="updatePaymentStatus()">Update Payment Status</button>

                            <div class="clearfix"> </div>

                            <div class="clearfix"> </div>


                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Reset Password -->

    <!-- Update Profile Status -->

    <div id="updateProfileStatus" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update Profile Status</h4>
                </div>
                <div class="modal-body">
                    <div class="login-w3ls">

                        <label>Profile Status</label>

                        <div class="w3_modal_body_grid">
                            <select id="w3_country" name="status" ng-model="status" class="frm-field required"
                                required="">
                                <option value="">Select Profile Status</option>
                                <option value="0">Just Registered</option>
                                <option value="1">Send Profile For Verification</option>
                                <option value="2">Profile Verified</option>
                                <option value="3">Verification Failed</option>
                                <option value="4">verification Pending</option>
                                <option value="5">deactivated</option>
                                <option value="6">married</option>
                            </select>
                        </div>



                        <br>
                        <br>
                        <button ng-click="updateProfileStatus()">Update Profile Status</button>

                        <div class="clearfix"> </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Update Profile Status -->

    <!-- Update Profile Photo -->

    <div id="updateProfilePhoto" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Update Profile Photo</h4>
                </div>
                <div class="modal-body">
                    <div class="login-w3ls">

                        <label>Profile Image</label>

                        <label class="file">
                            <input type='file' class="image-input" name="profile_pic" file-model="myFile"
                                id="profile_pic" required="" onchange="readURL(this);" />
                            <br>
                            <img id="blah" src="/images/profile_alt_image.png" alt="Profile Image" width="100"
                                height="100" />

                        </label>
                        <br>
                        <button ng-click="updateProfilePhoto()">Update Profile Photo</button>

                        <div class="clearfix"> </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Update Profile Photo -->

    <!-- Update Other Document -->

    <div id="uploadDocument" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Upload Documents</h4>
                </div>
                <div class="modal-body">
                    <div class="login-w3ls">

                        <label>Select Document</label>

                        <div class="w3_modal_body_grid">
                            <select id="w3_country" name="status" ng-model="docType" class="frm-field required"
                                required="">
                                <option value="">Select Document Type</option>
                                <option value="2">Jadhagam</option>
                                <option value="3">Pay Slip</option>
                                <option value="4">Community Certificate</option>
                                <option value="5">Property Images</option>
                                <option value="6">Others</option>
                            </select>
                        </div>


                        <label class="file">
                            <input type='file' class="image-input" name="document" file-model="myDocument" id="document"
                                required="" onchange="readURL(this);" />
                            <br>

                        </label>

                        <button ng-click="uploadDocument()">Upload Document</button>

                        <div class="clearfix"> </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Update Other Document -->


</div>


<script type="text/javascript">
var app = angular.module("profilePage", []);

app.directive('fileModel', ['$parse', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function() {
                scope.$apply(function() {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.controller("profileController", function($scope, $http) {

    $scope.familyDetails;
    $scope.patnerExpectation;

    $scope.profileDetails;
    $scope.transactionDetails;
    $scope.userDetails;
    $scope.userDocument;
    $scope.userMatchDetails;
    $scope.userOrders;
    $scope.userRole;
    $scope.userSubscription;
    $scope.activeStatus;
    $scope.planDetails;
    $scope.isAdmin = false;
    $scope.isOwnProfile = false;
    $scope.profileId;

    angular.element(window.document.body).ready(function() {

        $http.post('/Admin/getUserFullProfile', {
            "profileId": $scope.profileId,
        }).then(function(response) {

            console.log(response.data.data);
            $scope.familyDetails = response.data.data.familyDetails;
            $scope.patnerExpectation = response.data.data.patnerExpectation;

            $scope.profileDetails = response.data.data.profileDetails;
            $scope.transactionDetails = response.data.data.transactionDetails;
            $scope.userDetails = response.data.data.userDetails;
            $scope.userDocument = response.data.data.userDocument;
            $scope.userMatchDetails = response.data.data.userMatchDetails;
            $scope.userOrders = response.data.data.userOrders;
            $scope.userRole = response.data.data.userRole;
            $scope.userSubscription = response.data.data.userSubscription;
            $scope.planDetails = response.data.data.planDetails;
            $scope.activeStatus = response.data.data.activeStatus;
            $scope.isAdmin = response.data.data.isAdmin;
            $scope.isOwnProfile = response.data.data.isOwnProfile;
            $scope.profileId = response.data.data.profileId;

            switch (response.data.data.userDetails.is_active) {
                case '0':
                    $scope.activeStatus = 'Just Registered';
                    break;
                case '1':
                    $scope.activeStatus = 'Send Profile For Verification';
                    break;
                case '2':
                    $scope.activeStatus = 'Profile Verified';
                    break;
                case '3':
                    $scope.activeStatus = 'Verification Failed';
                    break;
                case '4':
                    $scope.activeStatus = 'verification Pending';
                    break;
                case '5':
                    $scope.activeStatus = 'deactivated';
                    break;
                case '6':
                    $scope.activeStatus = 'married';
                    break;
            }

        });

    });

    $scope.updatePaymentStatus = function() {
        var paymentStatus = $scope.paymentApprovalStatus;
        var userId = $scope.userDetails.id;

        if (paymentStatus == undefined || paymentStatus == null || paymentStatus == false ||
            paymentStatus == "") {
            alert("Select Status of the payment")
            return false;
        }

        $http.post('/Api/updatePaymentStatus', {
            "status": paymentStatus,
            "userId": userId
        }).then(function(response) {

            var item = response.data;
            alert(item.message);
            if (item.status) {
                $('#updatePayment').modal('hide');
            }

        });
    }

    $scope.uploadDocument = function() {

        var documentName = "";

        switch ($scope.docType) {
            case "2":
                documentName = "Jadhagam";
                break;
            case "3":
                documentName = "Pay Slip";
                break;
            case "4":
                documentName = "Community Certificate";
                break;
            case "5":
                documentName = "Property Images";
                break;
            case "6":
                documentName = "Others";
                break;
        }

        $http({
            method: 'POST',
            url: '/fileUpload',
            headers: {
                'Content-Type': undefined
            },
            data: {
                userfile: $scope.myDocument,
                fileType: $scope.docType,
                "documentName": documentName,
                "userName": $scope.userDetails.user_name
            },
            transformRequest: function(data, headersGetter) {
                var formData = new FormData();
                angular.forEach(data, function(value, key) {
                    formData.append(key, value);
                });
                return formData;
            }
        }).success(function(data, status, headers, config) {
            console.log(data, status, headers, config);

            if (data.status && status == 200) {
                alert(data.message);
                $('#uploadDocument').modal('hide');
            }
            //$window.location.href = '/Pages/view/login';

        }).error(function(data, status, headers, config) {
            console.log(data, status, headers, config);
            alert("Failed To upload " + documentName);
            //$window.location.href = '/Pages/view/login';
        });

    }

    $scope.updateProfilePhoto = function() {
        $http({
            method: 'POST',
            url: '/fileUpload',
            headers: {
                'Content-Type': undefined
            },
            data: {
                userfile: $scope.myFile,
                fileType: 1,
                "documentName": "Profile",
                "userName": $scope.userDetails.user_name
            },
            transformRequest: function(data, headersGetter) {
                var formData = new FormData();
                angular.forEach(data, function(value, key) {
                    formData.append(key, value);
                });
                return formData;
            }
        }).success(function(data, status, headers, config) {
            console.log(data, status, headers, config);

            if (data.status && status == 200) {

                alert(data.message);

                $('#updateProfilePhoto').modal('hide');

            }

            //$window.location.href = '/Pages/view/login';

        }).error(function(data, status, headers, config) {
            console.log(data, status, headers, config);
            alert("Failed To update Profile Photo");
            //$window.location.href = '/Pages/view/login';
        });

    }

    $scope.updateProfileStatus = function() {

        var status = $scope.status;
        var userId = $scope.userDetails.id;

        if (userId == undefined) {
            alert("Something Went Wrong");
            return false;
        }

        $http.post('/Api/updateProfileStatus', {
            "status": status,
            "userId": userId
        }).then(function(response) {

            var item = response.data;
            alert(item.message);
            if (item.status) {
                $('#updateProfileStatus').modal('hide');

            }

        });


    }

    $scope.changePassword = function() {

        console.log("Change Password");

        var oldPassword = $scope.oldPassword;
        var newPassword = $scope.newPassword;
        var userId = $scope.userDetails.id;

        if (oldPassword == newPassword) {
            alert("Please Enter Different Password")
            return false;
        }

        if (userId == undefined) {
            alert("Something Went Wrong");
            return false;
        }

        $http.post('/Api/changePassword', {
            "oldPassword": oldPassword,
            "newPassword": newPassword,
            "userId": userId
        }).then(function(response) {

            var item = response.data;
            alert(item.message);
            if (item.status) {
                $('#changePassword').modal('hide');
                $window.location.href = '/Pages/view/login';
            }

        });
    }

    $scope.resetPassword = function() {

        console.log("Reset Password");

        var newPassword = $scope.newPassword;
        var userId = $scope.userDetails.id;

        if (userId == undefined) {
            alert("Something Went Wrong");
            return false;
        }

        $http.post('/Api/resetProfilePassword', {
            "newPassword": newPassword,
            "userId": userId
        }).then(function(response) {

            var item = response.data;
            alert(item.message);
            if (item.status) {
                $('#resetProfilePassword').modal('hide');

            }

        });
    }

    $scope.updateFamilyDetails = function() {

        var userId = $scope.userDetails.id;

        if (userId == undefined) {
            alert("Something Went Wrong");
            return false;
        }

        $http.post('/Admin/upsertFamilyDetails', {
            "user_id": userId,
            "father_name": $scope.father_name,
            "mother_name": $scope.mother_name,
            "family_income": $scope.family_income,
            "father_occupation": $scope.father_occupation,
            "mother_occupation": $scope.mother_occupation,
            "family_description": $scope.family_description
        }).then(function(response) {

            var item = response.data;
            alert(item.message);
            if (item.status) {
                $('#familyEditModel').modal('hide');
            }

        });
    }

    $scope.updateDesirePatner = function() {

        var userId = $scope.userDetails.id;

        if (userId == undefined) {
            alert("Something Went Wrong");
            return false;
        }

        if ($scope.desire.height_from > $scope.desire.height_to) {
            alert("Enter Proper Height");
            return false;
        }

        if ($scope.desire.age_from > $scope.desire.age_to) {
            alert("Enter Proper Age");
            return false;
        }

        $http.post('/Admin/upsertDesirePatnerDetails', {
            "user_id": userId,
            "height_from": $scope.desire.height_from,
            "height_to": $scope.desire.height_to,
            "age_from": $scope.desire.age_from,
            "age_to": $scope.desire.age_to,
            "religion": $scope.desire.religion,
            "caste": $scope.desire.caste,
            "education": $scope.desire.education,
            "employment_status": $scope.desire.employment_status,
            "marital_status": $scope.desire.marital_status,
            "income_status": $scope.desire.income_status,
            "income_from": $scope.desire.income_from,
            "income_to": $scope.desire.income_to,
            "description": $scope.desire.description
        }).then(function(response) {

            var item = response.data;
            alert(item.message);
            if (item.status) {
                $('#userDesireEditModel').modal('hide');
            }

        });

    }

    $scope.updateLifeStyle = function() {
        var userId = $scope.userDetails.id;

        if (userId == undefined) {
            alert("Something Went Wrong");
            return false;
        }

        $http.post('/Admin/upsertLifeStyle', {
            "user_id": userId,
            "height": $scope.style.height,
            "weight": $scope.style.weight,
            "hobbies": $scope.style.hobbies,
            "own_house": $scope.style.own_house,
            "foodies": $scope.style.foodies,
            "smoker": $scope.style.smoker,
            "smoking_level": $scope.style.smoking_level,
            "drinker": $scope.style.drinker,
            "drinking_level": $scope.style.drinking_level
        }).then(function(response) {

            var item = response.data;
            alert(item.message);
            if (item.status) {
                $('#lifeStyleEditModel').modal('hide');
            }

        });
    }

    $scope.updateAboutMe = function() {
        var userId = $scope.userDetails.id;

        if (userId == undefined) {
            alert("Something Went Wrong");
            return false;
        }

        $http.post('/Admin/updateAboutMe', {
            "user_id": userId,
            "height": $scope.profile.height,
            "first_name": $scope.profile.first_name,
            "middle_name": $scope.profile.middle_name,
            "last_name": $scope.profile.last_name,
            "gender": $scope.profile.gender,
            "dob": $scope.profile.dob,
            "blood_group": $scope.profile.blood_group,
            "religion": $scope.profile.religion,
            "star": $scope.profile.star,
            "rasi": $scope.profile.rasi,
            "caste": $scope.profile.caste,
            "subcaste": $scope.profile.subcaste,
            "education": $scope.profile.education,
            "occupation": $scope.profile.occupation,
            "salary": $scope.profile.salary,
            "email_id": $scope.profile.email_id,
            "phone_no": $scope.profile.phone_no,
            "address": $scope.profile.address,
            "profile_description": $scope.profile.profile_description,
        }).then(function(response) {

            var item = response.data;
            alert(item.message);
            if (item.status) {
                $('#aboutMeEditModel').modal('hide');
            }

        });
    }

});
</script>