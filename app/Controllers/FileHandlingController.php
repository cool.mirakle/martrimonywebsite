<?php namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use \App\Models\AuthModel;
use \App\Models\AdminModel;
use \App\Models\FileHandlingModel;
use CodeIgniter\I18n\DateTime;
use CodeIgniter\Model;

class FileHandlingController extends \CodeIgniter\Controller
{
    use ResponseTrait;

    public function upload()
    {
        $filesDetails = $this->request->getPost();
        $file = $this->request->getFile('userfile');

        if ($file && !$file->isValid())
        {
                throw new RuntimeException($file->getErrorString().'('.$file->getError().')');
        }

        if(!$filesDetails['fileType']){
            return $this->fail(["message"=>"Invalid Request"]);
        }

        if(!$filesDetails['userName']){
            return $this->fail(["message"=>"User Details Not Found. Try Again"]);
        }

        $fileHandlingModel = new FileHandlingModel();

        $response = $fileHandlingModel->uploadFile($file,$filesDetails['fileType'],$filesDetails['documentName'],$filesDetails['userName']);

        return $this->respond($response);
    }

    public function paymentUpload()
    {
        $filesDetails = $this->request->getPost();

        $file = $this->request->getFile('paymentFile');

        if ($file && !$file->isValid())
        {
            throw new RuntimeException($file->getErrorString().'('.$file->getError().')');
        }

        if(!$filesDetails['reference']){
            return $this->fail(["message"=>"Invalid Details"]);
        }

        if(!$filesDetails['paidAmount']){
            return $this->fail(["message"=>"Invalid Details"]);
        }

        if(!$filesDetails['planDetails']){
            return $this->fail(["message"=>"Plan Details Not Selected. Try Again"]);
        }

        if(!$filesDetails['planId']){
            return $this->fail(["message"=>"Plan Details Not Selected. Try Again"]);
        }
        if(!$filesDetails['planAmount']){
            return $this->fail(["message"=>"Plan Details Not Selected. Try Again"]);
        }
        if(!$filesDetails['planName']){
            return $this->fail(["message"=>"Plan Details Not Selected. Try Again"]);
        }
            
        $fileHandlingModel = new FileHandlingModel();

        $response = $fileHandlingModel->paymentUpload($file,$filesDetails['reference'],$filesDetails['planId'],$filesDetails['planAmount'],$filesDetails['planName'],$filesDetails['paidAmount']);

        return $this->respond($response);
    }

    public function readFile(){
        
        $url = parse_url($_SERVER['REQUEST_URI']);

        parse_str($url['query'], $params);

        $fileName = $params['fileName'];

        try
        {
            if ($imagedata = file_get_contents(WRITEPATH."uploads/".$fileName))
            {
                return $imagedata;
            }
        }
        catch (\Exception $e)
        {
            return file_get_contents(WRITEPATH."uploads/"."profile_alt_image.png");
        }
        
        //$base64 = base64_encode($imagedata);

        return $imagedata;

    }

}