<?php namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use \App\Models\AuthModel;
use CodeIgniter\I18n\DateTime;
use CodeIgniter\HTTP\Response;
$session = \Config\Services::session();

class Api extends \CodeIgniter\Controller
{
    use ResponseTrait;

    public function login()
    {
        $loginData = $this->request->getJSON();

        $authModel = new AuthModel();
        //session_destroy();
        //session_start();      
        
        $response = service('response');
        $responseData = $authModel->authenticate($loginData->{'username'},$loginData->{'password'});

        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('X-API-Key', $responseData['token']);
        
        $response->noCache();

        unset($responseData['token']);
        
        $response->setBody(json_encode($responseData));
        
        $response->setStatusCode(Response::HTTP_OK);
           
        $response->send();
    }

    public function logout(){

        $authModel = new AuthModel();
        
        $response = service('response');

        $responseData = $authModel->logout();
        
        $response->setBody(json_encode($responseData));

        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('X-API-Key', "");
        
        $response->noCache();

        $response->setStatusCode(Response::HTTP_OK);
           
        $response->send();
    }

    public function register(){
        
        $registerData = $this->request->getJSON();

        $authModel = new AuthModel();

        $response = $authModel->registerUser($registerData);
        
        return $this->respondCreated($response);

    }

    public function forgotPassword(){
        
        $data = $this->request->getJSON();
        $authModel = new AuthModel();

        if($data && empty($data->{'username'})){ 
            return $this->respondBadRequest(["message"=>"Provide Valid Information.","status"=>FALSE,"token"=>$jwtToken]);
        } else {

            $response = $authModel->forgotPassword($data);

        }

        return $this->respondCreated($response);

    }

    public function changePassword(){
        $data = $this->request->getJSON();

        $authModel = new AuthModel();
        
        if($data->{'oldPassword'} != $data->{'newPassword'}){

            $response = $authModel->changePasswordByUserId($data->{'userId'},$data->{'oldPassword'},$data->{'newPassword'});

            return $this->respond($response);
        } else {
            return $this->respond(["message"=>"Password Should Not be same","status"=>FALSE,"data"=>[]]);
        }

    }

    public function resetProfilePassword(){
        
        $data = $this->request->getJSON();

        $authModel = new AuthModel();
        
        $response = $authModel->resetProfilePasswordById($data->{'userId'},$data->{'newPassword'});

        return $this->respond($response);
        
    }

    public function updateProfileStatus(){
        
        $data = $this->request->getJSON();

        $authModel = new AuthModel();
        
        $response = $authModel->resetProfileStatus($data->{'userId'},$data->{'status'});

        return $this->respond($response);
        
    }

    public function updatePaymentStatus(){

        $data = $this->request->getJSON();

        $authModel = new AuthModel();
    


        // if($data->{'status'}==0 || $data->{'status'}=="" || $data->{'status'}==undefined || $data->{'status'} == null){

        //     $response = ["message"=>"Check The Status","status"=>FALSE];
        // } else {

            $response = $authModel->updatePaymentStatus($data->{'userId'},$data->{'status'});
        //}
        return $this->respond($response);

    }

}



