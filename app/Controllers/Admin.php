<?php namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use \App\Models\AuthModel;
use \App\Models\AdminModel;
use \App\Models\CommonModel;
use \App\Models\SessionModel;
use CodeIgniter\I18n\DateTime;
use CodeIgniter\Model;

class Admin extends \CodeIgniter\Controller
{
    use ResponseTrait;

    public function userDetails(){
        
        $adminModel = new AdminModel();
        
        $response = $adminModel->getAllUserDetails();
        
        return $this->respond($response,200);

    }

    public function getPlanDetails(){

        $data = $this->request->getJSON();

        $adminModel = new AdminModel();
        
        $response = $adminModel->getActivePlanDetails();

        return $this->respond($response,200);

    }

    public function updateSubscription(){
        
        $data = $this->request->getJSON();

        $planDetails = $data->{'planDetails'};

        $adminModel = new AdminModel();

        $response = $adminModel->updateSubscriptionDetails($planDetails);

        return $this->respond($response,200);
        
    }

    public function getUserFullProfile(){

        $data = $this->request->getJSON();

        $profileId = $data->{'profileId'};

        if(!$profileId){
            $sessionModel = new SessionModel();
            $profileId = $sessionModel->getProfileIdFromSession();
        }

        if( isset( $profileId ) && !empty( $profileId ) ){

            $adminModel = new AdminModel();
            
            $response = $adminModel->getUserFullProfile($profileId);
    
            return $this->respond($response,200);

        } else {
         
            return $this->respond(["message"=>"Failure","status"=>FALSE,"data"=>[]],200);

        }

    }
    
    public function upsertFamilyDetails(){

        $data = $this->request->getJSON();

        if( isset( $data->{'user_id'} ) && !empty( $data->{'user_id'} ) ){

            $adminModel = new AdminModel();
            $insertionFields =[
                "user_id"=>$data->{'user_id'},
                "father_name"=>$data->{'father_name'},
                "mother_name"=>$data->{'mother_name'},
                "family_income"=>$data->{'family_income'},
                "father_occupation"=>$data->{'father_occupation'},
                "mother_occupation"=>$data->{'mother_occupation'},
                "family_description"=>$data->{'family_description'}
            ];

            $response = $adminModel->upsertFamilyDetail($insertionFields);
    
            return $this->respond($response,200);

        } else {
         
            return $this->respond(["message"=>"Failure","status"=>FALSE,"data"=>[]],400);

        }
    }

    public function upsertDesirePatnerDetails(){

        $data = $this->request->getJSON();

        if( isset( $data->{'user_id'} ) && !empty( $data->{'user_id'} ) ){

            $adminModel = new AdminModel();

            $insertionFields =[
                "user_id"=>$data->{'user_id'},
                "age_from"=>$data->{'age_from'},
                "age_to"=>$data->{'age_to'},
                "height_from"=>$data->{'height_from'},
                "height_to"=>$data->{'height_to'},
                "religion"=>$data->{'religion'},
                "caste"=>$data->{'caste'},
                "education"=>$data->{'education'},
                "employment_status"=>$data->{'employment_status'},
                "marital_status"=>$data->{'marital_status'},
                "income_status"=>$data->{'income_status'},
                "income_from"=>$data->{'income_from'},
                "income_to"=>$data->{'income_to'},
                "description"=>$data->{'description'}
            ];

            $response = $adminModel->upsertDesirePatnerDetails($insertionFields);
    
            return $this->respond($response,200);

        } else {
         
            return $this->respond(["message"=>"Failure","status"=>FALSE,"data"=>[]],400);

        }
    }

    public function upsertLifeStyle(){

        $data = $this->request->getJSON();

        if( isset( $data->{'user_id'} ) && !empty( $data->{'user_id'} ) ){

            $adminModel = new AdminModel();

            $insertionFields =[
                "user_id"=>$data->{'user_id'},
                "height"=>$data->{'height'},
                "weight"=>$data->{'weight'},
                "hobbies"=>$data->{'hobbies'},
                "own_house"=>$data->{'own_house'},
                "foodies"=>$data->{'foodies'},
                "smoker"=>$data->{'smoker'},
                "smoking_level"=>$data->{'smoking_level'},
                "drinker"=>$data->{'drinker'},
                "drinking_level"=>$data->{'drinking_level'}
            ];

           
            $response = $adminModel->updateUserProfile($insertionFields);
    
           
            return $this->respond($response,200);

        } else {
         
            return $this->respond(["message"=>"Failure","status"=>FALSE,"data"=>[]],400);

        }
    }

    public function updateAboutMe(){

        $data = $this->request->getJSON();

        if( isset( $data->{'user_id'} ) && !empty( $data->{'user_id'} ) ){

            $adminModel = new AdminModel();

            $insertionFields =[
                "user_id"=>$data->{'user_id'},
                "first_name"=>$data->{'first_name'},
                "middle_name"=>$data->{'middle_name'},
                "last_name"=>$data->{'last_name'},
                "gender"=>$data->{'gender'},
                "dob"=>$data->{'dob'},
                "blood_group"=>$data->{'blood_group'},
                "religion"=>$data->{'religion'},
                "star"=>$data->{'star'},
                "rasi"=>$data->{'rasi'},
                "caste"=>$data->{'caste'},
                "subcaste"=>$data->{'subcaste'},
                "education"=>$data->{'education'},
                "occupation"=>$data->{'occupation'},
                "salary"=>$data->{'salary'},
                "phone_no"=>$data->{'phone_no'},
                "email_id"=>$data->{'email_id'},
                "address"=>$data->{'address'},
                "profile_description"=>$data->{'profile_description'}

            ];

            $commonModel = new CommonModel();

            if($commonModel->checkAge($data->{'gender'},$data->{'dob'})){

                $response = $adminModel->updateUserProfile($insertionFields);
    
            } else {
                $response['status'] = False;
                $response['message'] = 'Enter valid Date Of Birth';
                $response['data'] = [];
            }
    
            return $this->respond($response,200);

        } else {
         
            return $this->respond(["message"=>"Failure","status"=>FALSE,"data"=>[]],400);

        }
    }

}