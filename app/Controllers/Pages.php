<?php namespace App\Controllers;

use CodeIgniter\Controller;

class Pages extends Controller
{

	protected $session;


    function __construct()
    {

        $this->session = \Config\Services::session();
        $this->session->start();
		
    }

    public function index()
    {
        echo view('templates/header');

        echo view('/pages/login');

        echo view('templates/footer');
    }

    public function view($page = 'home')
    {

        if ( ! is_file(APPPATH.'/Views/pages/'.$page.'.php'))
        {
            throw new \CodeIgniter\Exceptions\PageNotFoundException($page);
        }
    
        $data['title'] = ucfirst($page); // Capitalize the first letter
    
        echo view('templates/header', $data);

        $url = parse_url($_SERVER['REQUEST_URI']);

        if($page == 'Profile' || $page == 'profile'){
            try {
                parse_str($url['query'], $params);
                echo view('pages/'.$page, ["profileId"=>$params['profileId']]);
             } catch (\Throwable $th) {

                 echo view('pages/'.$page, ["profileId"=>null]);
             }

            //echo view('pages/'.$page, ["profileId"=>$params['profileId']]);

        } else {

            echo view('pages/'.$page);

        }

        // if($page == 'profile'){
        //     echo view('family_details');
        // }

        echo view('templates/footer', $data);

    }
}